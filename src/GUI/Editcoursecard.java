package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import data.Course;
import utilities.Validators;

/**
 * Class is responsible of creating a JPanel enabling editing of courses and its part.
 * @author rmy002
 *
 */
public class Editcoursecard extends JPanel {
	private final String header = "Course";
	private JTextField courseName, courseDescription, description, groupSize, gradeWeight, partId;
	private JPanel contentPanel, partPanel, infoPanel, innerInfoPanel;
	private JLabel totalGradeWeight;
	private JButton saveButton, cancelButton;
	private JScrollPane scroll;
	private Listener listen;
	private ArrayList<JPanel> partPanels;

	/**
	 * Constructor for the class Editcoursecard
	 * @param listen, listener for the card
	 */
	public Editcoursecard(Listener listen) {
		this.listen = listen;
		this.partPanels = new ArrayList<>();
		initCourseCard();

	}
	
	/**
	 * Sets up the main panel
	 */
	private void initCourseCard() {

		setLayout(new BorderLayout());

		setupInfoPanel();

		contentPanel = new JPanel();
		scroll = new JScrollPane(contentPanel);
		scroll.setBorder(null);
		contentPanel.setLayout(new BorderLayout());
		totalGradeWeight = new JLabel("");

		initCoursePanel();

		partPanel = new JPanel();
		partPanel.setLayout(new BoxLayout(partPanel, BoxLayout.PAGE_AXIS));
		partPanel.setBackground(Color.white);
		contentPanel.add(partPanel, BorderLayout.CENTER);

		setupDefaultPartPanels();

		add(scroll, BorderLayout.CENTER);

	}
	
	/**
	 * Sets up a info panel on the right side of the card showing course parts
	 */
	private void setupInfoPanel() {

		infoPanel = new JPanel();
		add(infoPanel, BorderLayout.EAST);
		infoPanel.setBackground(Color.white);
		infoPanel.setPreferredSize(new Dimension(200, 300));
		infoPanel.setLayout(new FlowLayout());

		infoPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.orange));

		innerInfoPanel = new JPanel();
		innerInfoPanel.setBackground(Color.white);
		infoPanel.add(innerInfoPanel);

		JPanel infoButtonPanel = new JPanel();
		infoButtonPanel.setBackground(Color.white);
		saveButton = new JButton("Save");
		saveButton.addActionListener(listen);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(listen);
		cancelButton.setActionCommand("Overview");
		infoButtonPanel.add(saveButton);
		infoButtonPanel.add(cancelButton);

		infoPanel.add(infoButtonPanel);

	}
	
	/**
	 * Methods creates a label with total weight grade of a given course
	 * @param course, the given course
	 * @return the label with total grade weight of the course
	 */
	private JLabel createTotalWeightLabel(Course course) {
		String prefix = "Total Grade Weight: ";

		if (course.checkWeightGrade()) {
			totalGradeWeight = new JLabel("<html>" + prefix + course.getTotalGradeWeight() + "%</html>");
		} else {
			totalGradeWeight = new JLabel(
					"<html>" + prefix + "<font color=red>" + course.getTotalGradeWeight() + "%</font></html>");
		}
		return totalGradeWeight;

	}
	
	/**
	 * Sets up a panel showing the basic information of the course, but not its parts
	 */
	private void initCoursePanel() {

		JPanel outerCoursePanel = new JPanel(new BorderLayout());
		outerCoursePanel.setBackground(Color.white);
		contentPanel.add(outerCoursePanel, BorderLayout.NORTH);
		JPanel coursePanel = new JPanel(new GridLayout(2, 2));
		coursePanel.setPreferredSize(new Dimension(650, 125));
		coursePanel.setBackground(Color.white);
		outerCoursePanel.add(coursePanel, BorderLayout.NORTH);
		coursePanel.setBorder(BorderFactory.createEmptyBorder(10, 5, 20, 0));

		coursePanel.add(new JLabel("Course name:"));
		courseName = new JTextField();
		coursePanel.add(courseName);

		coursePanel.add(new JLabel("Description:"));
		courseDescription = new JTextField();
		coursePanel.add(courseDescription);

		JLabel coursePart = new JLabel("Course Parts:");
		coursePart.setFont(new Font("Verdana", Font.ITALIC, 20));
		outerCoursePanel.add(coursePart, BorderLayout.SOUTH);

	}

	
	/**
	 * Method creates a blank panel for showing course part information
	 * @return the new panel
	 */
	private JPanel initPartPanel() {

		JPanel inputPanel = new JPanel(new GridLayout(4, 2));
		inputPanel.setPreferredSize(new Dimension(650, 150));
		inputPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 150));
		inputPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.orange));

		inputPanel.add(new JLabel("Description:"));
		description = new JTextField();
		description.setName("desc");
		inputPanel.add(description);

		inputPanel.add(new JLabel("Group Size:"));
		groupSize = new JTextField();
		groupSize.setName("size");
		inputPanel.add(groupSize);

		inputPanel.add(new JLabel("Grade weight:"));
		gradeWeight = new JTextField();
		gradeWeight.setName("weight");
		inputPanel.add(gradeWeight);

		return inputPanel;

	}
	
	/**
	 * Clear Jtextfields for new input
	 */
	protected void clearFields() {
		this.courseName.setText("");
		this.courseDescription.setText("");
	}
	
	/**
	 * Methods creates a button panel for the course parts
	 * @return
	 */
	private JPanel getButtonPanel() {
		JPanel submitPanel = new JPanel();
		submitPanel.setBackground(Color.white);
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.RIGHT);
		submitPanel.setLayout(layout);
		return submitPanel;

	}
	
	/**
	 * Method creates a panel meant for creating a new part of the course
	 */
	protected void setupNewInputPanel() {
		JPanel inputPanel = initPartPanel();
		inputPanel.setName("0");
		inputPanel.setBackground(new Color(250, 250, 250));

		JButton addPart = new JButton("Add part");
		addPart.addActionListener(listen);
		addPart.setActionCommand("newPart");
		JPanel submitPanel = getButtonPanel();
		submitPanel.add(addPart);

		inputPanel.add(submitPanel);
		partPanels.add(inputPanel);

	}
	
	/**
	 * Methods clears all existing part panels in preparation for a new course
	 */
	protected void setupDefaultPartPanels() {
		partPanels.clear();
		setupNewInputPanel();
	}

	/**
	 * Find a part panel of a given course part
	 * @param id of the part
	 * @return the panel found
	 */
	protected JPanel findJpanelById(String id) {
		for (JPanel panel : partPanels) {
			if (panel.getName().equals(id))
				return panel;
		}
		return null;

	}
	
	/**
	 * Creates a part panel from an already existing part with its respective information
	 * @param id of the part
	 * @param partDescription, description of the part
	 * @param size, the maximum group size of the part
	 * @param weight, the grade weight of the part
	 */
	protected void setupInputPanel(int id, String partDescription, int size, int weight) {

		JPanel inputPanel = initPartPanel();
		inputPanel.setName(String.valueOf(id));

		inputPanel.setBackground(Color.white);
		for (Component c : inputPanel.getComponents()) {
			if (c instanceof JTextField) {

				if (c.getName().equals("desc"))
					((JTextField) c).setText(partDescription);
				else if (c.getName().equals("size"))
					((JTextField) c).setText(String.valueOf(size));
				else
					((JTextField) c).setText(String.valueOf(weight));

			}
		}

		JPanel submitPanel = getButtonPanel();

		inputPanel.setBackground(Color.white);
		inputPanel.add(submitPanel);
		partPanels.add(inputPanel);

	}

	/**
	 * Refreshes the content panel, adding all stored part panels 
	 */
	public void paintPartPanels() {
		partPanel.removeAll();

		for (int i = 1; i < partPanels.size(); i++) {
			partPanel.add(partPanels.get(i));
		}
		
		for(Component c : partPanels.get(0).getComponents()) {
			if(c instanceof JTextField) {
				((JTextField) c).setText("");
			}
		}
		
		partPanel.add(partPanels.get(0));
		partPanel.revalidate();
		
		
	}
	
	/**
	 * Refreshes the info panel with the given course
	 * @param course, the course selected
	 */
	protected void refreshInfoPanel(Course course) {
		innerInfoPanel.removeAll();

		JPanel partGrid = new JPanel(new GridLayout(7, 1));
		partGrid.setBackground(Color.white);
		
		if(course != null) {
		for (int i = 0; i < course.getParts().size(); i++) {

			JLabel infoPartLabel = new JLabel("<html><body>Part " + (i + 1) + " <br>Grade Weight: "
					+ course.getParts().get(i).getGradeWeight() + "%</body></html>");
			partGrid.add(infoPartLabel);
		}
		partGrid.add(new JSeparator());

		partGrid.add(createTotalWeightLabel(course));
		}

		innerInfoPanel.add(partGrid);
		innerInfoPanel.revalidate();

	}
	
	/**
	 * Gets information from all active part panels stored in the card 
	 * @return all parts as a 2d array
	 */
	public String[][] getPartInfo() {
		String[][] parts = new String[partPanels.size() - 1][];
		for (int i = 1; i < partPanels.size(); i++) {
			parts[i - 1] = getInputText(i);
		}
		return parts;
	}

	/**
	 * Gets information from a given jpanel 
	 * @param id of the panel/part
	 * @return the part information as an array
	 */
	protected String[] getInputText(int id) {
		JPanel currentPanel = partPanels.get(id);
		String partId = currentPanel.getName();
		String description = "";
		String groupSize = "";
		String gradeWeight = "";

		for (Component c : currentPanel.getComponents()) {
			if (c instanceof JTextField) {
				if (c.getName().equals("desc")) {
					description = (String) ((JTextField) c).getText();
				} else if (c.getName().equals("size")) {
					groupSize = ((JTextField) c).getText();
				} else {
					gradeWeight = ((JTextField) c).getText();
				}
			}
		}
		return new String[] { partId, description, groupSize, gradeWeight };
	}


	/**
	 * @return the description
	 */
	public JTextField getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(JTextField description) {
		this.description = description;
	}

	/**
	 * @return the groupSize
	 */
	public JTextField getGroupSize() {
		return groupSize;
	}

	/**
	 * @param groupSize
	 *            the groupSize to set
	 */
	public void setGroupSize(JTextField groupSize) {
		this.groupSize = groupSize;
	}

	/**
	 * @return the gradeWeight
	 */
	public JTextField getGradeWeight() {
		return gradeWeight;
	}

	/**
	 * @param gradeWeight
	 *            the gradeWeight to set
	 */
	public void setGradeWeight(JTextField gradeWeight) {
		this.gradeWeight = gradeWeight;
	}


	/**
	 * @return the courseName
	 */
	public JTextField getCourseName() {
		return courseName;
	}

	/**
	 * @param courseName
	 *            the courseName to set
	 */
	public void setCourseName(JTextField courseName) {
		this.courseName = courseName;
	}

	/**
	 * @return the courseDescription
	 */
	public JTextField getCourseDescription() {
		return courseDescription;
	}

	/**
	 * @param courseDescription
	 *            the courseDescription to set
	 */
	public void setCourseDescription(JTextField courseDescription) {
		this.courseDescription = courseDescription;
	}

	/**
	 * @return the contentPanel
	 */
	public JPanel getContentPanel() {
		return contentPanel;
	}

	/**
	 * @param contentPanel
	 *            the contentPanel to set
	 */
	public void setContentPanel(JPanel contentPanel) {
		this.contentPanel = contentPanel;
	}

	/**
	 * @return the partPanel
	 */
	public JPanel getPartPanel() {
		return partPanel;
	}

	/**
	 * @param partPanel
	 *            the partPanel to set
	 */
	public void setPartPanel(JPanel partPanel) {
		this.partPanel = partPanel;
	}

	/**
	 * @return the scroll
	 */
	public JScrollPane getScroll() {
		return scroll;
	}

	/**
	 * @param scroll
	 *            the scroll to set
	 */
	public void setScroll(JScrollPane scroll) {
		this.scroll = scroll;
	}

	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}

	/**
	 * @param listen
	 *            the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}

	/**
	 * @return the partPanels
	 */
	public ArrayList<JPanel> getPartPanels() {
		return partPanels;
	}

	/**
	 * @param partPanels
	 *            the partPanels to set
	 */
	public void setPartPanels(ArrayList<JPanel> partPanels) {
		this.partPanels = partPanels;
	}


	/**
	 * @return the totalGradeWeight
	 */
	public JLabel getTotalGradeWeight() {
		return totalGradeWeight;
	}

	/**
	 * @param totalGradeWeight
	 *            the totalGradeWeight to set
	 */
	public void setTotalGradeWeight(JLabel totalGradeWeight) {
		this.totalGradeWeight = totalGradeWeight;
	}

	/**
	 * @return the infoPanel
	 */
	public JPanel getInfoPanel() {
		return infoPanel;
	}

	/**
	 * @param infoPanel
	 *            the infoPanel to set
	 */
	public void setInfoPanel(JPanel infoPanel) {
		this.infoPanel = infoPanel;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @return the innerInfoPanel
	 */
	public JPanel getInnerInfoPanel() {
		return innerInfoPanel;
	}

	/**
	 * @param innerInfoPanel
	 *            the innerInfoPanel to set
	 */
	public void setInnerInfoPanel(JPanel innerInfoPanel) {
		this.innerInfoPanel = innerInfoPanel;
	}

	/**
	 * @return the saveButton
	 */
	public JButton getSaveButton() {
		return saveButton;
	}

	/**
	 * @param saveButton
	 *            the saveButton to set
	 */
	public void setSaveButton(JButton saveButton) {
		this.saveButton = saveButton;
	}

	/**
	 * @return the cancelButton
	 */
	public JButton getCancelButton() {
		return cancelButton;
	}

	/**
	 * @param cancelButton
	 *            the cancelButton to set
	 */
	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	/**
	 * @return the partId
	 */
	public JTextField getPartId() {
		return partId;
	}

	/**
	 * @param partId
	 *            the partId to set
	 */
	public void setPartId(JTextField partId) {
		this.partId = partId;
	}

}

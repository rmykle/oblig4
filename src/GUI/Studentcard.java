package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import data.Course;
import data.GUIconverters;
import data.Student;

public class Studentcard extends JPanel {

	private Listener listen;
	private final String header = "Students";
	private JList<Object> studentlist;
	private JButton addStudent, deleteStudent, saveCourses, undoAll, gradeStudent, searchForStudent;
	private JTable studentCourses;
	private String[] columns = { "Course", "Attending" };

	public Studentcard(Listener listen) {
		this.listen = listen;
		setLayout(new GridLayout(1, 2));
		setupStudentPanel();
		setupStudentCourses();

	}

	
	/**
	 * Sets up the right side of the studentcard showing all courses available to the student
	 */
	private void setupStudentCourses() {
		JPanel coursePanel = new JPanel(new BorderLayout());
		coursePanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.orange));

		JLabel courseLabel = new JLabel("Courses");
		courseLabel.setOpaque(true);
		courseLabel.setBackground(Color.orange);
		coursePanel.add(courseLabel, BorderLayout.NORTH);

		Object[][] testdata = {};

		DefaultTableModel model = new DefaultTableModel(testdata, columns) {
			@Override
			public Class getColumnClass(int column) {
				switch (column) {
				case 1:
					return getValueAt(1, column).getClass();

				default:
					return getValueAt(0, column).getClass();
				}

			}
		};

		JPanel studentCourseButtons = new JPanel(new FlowLayout());
		studentCourseButtons.setBackground(Color.orange);
		coursePanel.add(studentCourseButtons, BorderLayout.SOUTH);
		
		searchForStudent = new JButton("Search for student");
		searchForStudent.addActionListener(listen);
		searchForStudent.setActionCommand("Grade student");
		studentCourseButtons.add(searchForStudent);
		
		saveCourses = new JButton("Save changes");
		saveCourses.addActionListener(listen);
		studentCourseButtons.add(saveCourses);

		undoAll = new JButton("Undo all changes");
		undoAll.addActionListener(listen);
		studentCourseButtons.add(undoAll);

		studentCourses = new JTable(model);
		coursePanel.add(studentCourses);
		add(coursePanel);

	}
	
	/**
	 * Sets up the left side of the card showing a list of all students
	 */
	private void setupStudentPanel() {
		JPanel studentPanel = new JPanel(new BorderLayout());
		studentPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.orange));

		JLabel studentLabel = new JLabel("Students");
		studentLabel.setBackground(Color.orange);
		studentLabel.setOpaque(true);
		studentPanel.add(studentLabel, BorderLayout.NORTH);

		Object[] studentList = GUIconverters.getStudentAsArray();
		studentlist = new JList<>(studentList);
		studentlist.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateTable();
			}
		});

		studentPanel.add(studentlist, BorderLayout.CENTER);

		JPanel studentButtons = new JPanel(new FlowLayout());
		studentButtons.setBackground(Color.orange);
		addStudent = new JButton("Add student");
		addStudent.addActionListener(listen);
		deleteStudent = new JButton("Delete Student");
		deleteStudent.addActionListener(listen);

		gradeStudent = new JButton("Grade student");
		gradeStudent.addActionListener(listen);
		gradeStudent.setActionCommand("Grade student");
		
		studentButtons.add(gradeStudent);
		studentButtons.add(addStudent);
		studentButtons.add(deleteStudent);
		studentPanel.add(studentButtons, BorderLayout.SOUTH);

		add(studentPanel);

	}
	
	/**
	 * Updates the course table based on the input of the student list
	 */
	protected void updateTable() {
		DefaultTableModel model = (DefaultTableModel) studentCourses.getModel();
		Student student = (Student) studentlist.getSelectedValue();
		if (student != null) {
			model.setDataVector(GUIconverters.getStudentCardCourseOverview(student), columns);
			model.fireTableDataChanged();
		}
	}
	
	/**
	 * Methods updates the student list upon new additions or delete
	 */
	protected void updateStudentList() {
		Object[] students = listen.getStorage().getStudents().values().toArray();
		DefaultListModel<Object> model = new DefaultListModel<>();
		for (int i = 0; i < students.length; i++) {
			model.addElement(students[i]);
		}
		studentlist.setModel(model);
	}

	/**
	 * Methods returns an arraylist of all marked courses in the table
	 * @return list of marked courses
	 */
	protected ArrayList<Course> getCheckedCourses() {
		ArrayList<Course> checkedCourses = new ArrayList<>();
		for (int i = 0; i < studentCourses.getRowCount(); i++) {
			boolean marked = (boolean) studentCourses.getValueAt(i, 1);
			if (marked == true) {
				checkedCourses.add((Course) studentCourses.getValueAt(i, 0));
			}
		}
		return checkedCourses;
	}

	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}

	/**
	 * @param listen
	 *            the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @return the addStudent
	 */
	public JButton getAddStudent() {
		return addStudent;
	}

	/**
	 * @param addStudent
	 *            the addStudent to set
	 */
	public void setAddStudent(JButton addStudent) {
		this.addStudent = addStudent;
	}

	/**
	 * @return the deleteStudent
	 */
	public JButton getDeleteStudent() {
		return deleteStudent;
	}

	/**
	 * @param deleteStudent
	 *            the deleteStudent to set
	 */
	public void setDeleteStudent(JButton deleteStudent) {
		this.deleteStudent = deleteStudent;
	}

	/**
	 * @return the studentCourses
	 */
	public JTable getStudentCourses() {
		return studentCourses;
	}

	/**
	 * @param studentCourses
	 *            the studentCourses to set
	 */
	public void setStudentCourses(JTable studentCourses) {
		this.studentCourses = studentCourses;
	}

	/**
	 * @return the saveCourses
	 */
	public JButton getSaveCourses() {
		return saveCourses;
	}

	/**
	 * @param saveCourses
	 *            the saveCourses to set
	 */
	public void setSaveCourses(JButton saveCourses) {
		this.saveCourses = saveCourses;
	}

	/**
	 * @return the undoAll
	 */
	public JButton getUndoAll() {
		return undoAll;
	}

	/**
	 * @param undoAll
	 *            the undoAll to set
	 */
	public void setUndoAll(JButton undoAll) {
		this.undoAll = undoAll;
	}

	/**
	 * @return the columns
	 */
	public String[] getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	/**
	 * @return the gradeStudent
	 */
	public JButton getGradeStudent() {
		return gradeStudent;
	}

	/**
	 * @param gradeStudent
	 *            the gradeStudent to set
	 */
	public void setGradeStudent(JButton gradeStudent) {
		this.gradeStudent = gradeStudent;
	}

	/**
	 * @return the studentlist
	 */
	public JList<Object> getStudentlist() {
		return studentlist;
	}

	/**
	 * @param studentlist
	 *            the studentlist to set
	 */
	public void setStudentlist(JList<Object> studentlist) {
		this.studentlist = studentlist;
	}

	/**
	 * @return the searchForStudent
	 */
	public JButton getSearchForStudent() {
		return searchForStudent;
	}

	/**
	 * @param searchForStudent the searchForStudent to set
	 */
	public void setSearchForStudent(JButton searchForStudent) {
		this.searchForStudent = searchForStudent;
	}

}

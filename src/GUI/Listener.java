package GUI;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import data.Course;
import data.Coursepart;
import data.GUIconverters;
import data.Gradecalculator;
import data.Storageunit;
import data.Student;
import data.Studentgroups;
import enums.Grade;
import sqlhandling.DAOfactory;
import sqlhandling.SqlDAOFactory;
import utilities.Validators;

public class Listener implements ActionListener {
	private GUI gui;
	private Storageunit storage;
	private Course currentCourse;
	private DAOfactory daoFactory;
	private ArrayList<String> gradeChanges;

	public Listener(Storageunit storage) {

		this.storage = storage;
		this.gradeChanges = new ArrayList<>();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("newPart")) {
			createNewPart();
		}

		else if (e.getActionCommand().equals(gui.getOverviewCard().getHeader())) {
			gui.getOverviewCard().updateCourseList();
			changeCard(e);
			
		}
		else if(e.getSource().equals(gui.getOverviewCard().getOpenCourse())){
			openCourse(e);
		}
		else if(e.getSource().equals(gui.getStudentCard().getSearchForStudent())) {
			searchForStudent(e);
		}
		else if(e.getActionCommand().equals(gui.getStudentCard().getHeader())) {
			changeCard(e);
		}
		
		else if(e.getSource().equals(gui.getStudentCard().getAddStudent())) {
			addStudent();
		}

		else if (e.getSource().equals(gui.getEditCard().getSaveButton())) {
			saveCourse();
		}
		else if(e.getSource().equals(gui.getStudentCard().getDeleteStudent())) {
			deleteStudent();
		}
		
		else if(e.getSource().equals(gui.getStudentCard().getSaveCourses())) {
			saveStudentCourses();
		}
		else if(e.getSource().equals(gui.getStudentCard().getUndoAll())) {
			undoStudentCourse();			
		}
		else if(e.getActionCommand().equals(gui.getGradeCard().getHeader())) {
			changeCard(e);
		}
		else if(e.getSource().equals(gui.getOverviewCard().getAddCourse())) {
			currentCourse = null;
			gui.getEditCard().clearFields();
			
				gui.getEditCard().setupDefaultPartPanels();
				updateCourseInput();
				changeCard(e);
				
			
			changeCard(e);
		}
		else if(e.getSource().equals(gui.getGradeCard().getCourseBox())) {
			updateCoursePartBox();
		}
		else if(e.getSource().equals(gui.getGradeCard().getCoursePartBox())) {
			editCourseTable();
		}
		else if(e.getActionCommand().equals(gui.getStudentGradeCard().getHeader())) {
			if(gui.getStudentCard().getStudentlist().getSelectedIndex() != -1) {
				Student selectedStudent = (Student) gui.getStudentCard().getStudentlist().getSelectedValue();
				setupStudentGradeCard(selectedStudent, e);
			}
			
		}
		else if(e.getSource().equals(gui.getStudentGradeCard().getSaveButton())) {
			saveGrades();
		}
		else if(e.getSource().equals(gui.getStudentGradeCard().getCancelButton())) {
			gradeChanges.clear();
			changeCard(e);
		}
		else if(e.getActionCommand().equals("gradeCombo")) {
			JComboBox<String> gradeBox = (JComboBox<String>) e.getSource();
			
			gradeChanges.add(gradeBox.getName()+"|"+ gradeBox.getSelectedItem().toString());
		}
		else if(e.getSource().equals(gui.getGradeCard().getNewGroup())) {
			setupGroupCreatorFrame(false);
		}
		else if(e.getSource().equals(gui.getGradeCard().getEditGroup())) {
			
			setupGroupCreatorFrame(true);
			
		}
		else if(e.getActionCommand().equals("closeGroupWindow")) {
			gui.getgFrame().dispose();
			gui.setgFrame(null);
		}
		
		else if(e.getActionCommand().equals("saveGroup")) {
			saveNewGroup();
		}
		
		else if(e.getSource().equals(gui.getGradeCard().getSaveCourses())) {
			saveGradesByTable();
		}
		else if(e.getSource().equals(gui.getOverviewCard().getDeleteCourse())) {
			deleteCourse();
		}
		
	}	
	
	private void setupStudentGradeCard(Student selectedStudent, ActionEvent e) {
		
		
			gui.getStudentGradeCard().getStudentName().setText(selectedStudent.getName());
			JPanel newPanel = GUIconverters.setupCoursePanel(selectedStudent, this);
			gui.getStudentGradeCard().getCourseScroll().setViewportView(newPanel);
			gui.getStudentGradeCard().getInfoPanel().removeAll();
			gui.getStudentGradeCard().getInfoPanel().add(GUIconverters.updateStudentGradeCardInfoPanel(selectedStudent));
			changeCard(e);
			
		
	}

	private void deleteCourse() {
		if(gui.getOverviewCard().getCourseList().getSelectedIndex() != -1) {
			Course selectedCourse = (Course) gui.getOverviewCard().getCourseList().getSelectedValue();
			if(JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedCourse.getCourseName() +"?", "Delete course", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				storage.deleteCourse(selectedCourse);
				daoFactory.getCourseDAO().deleteInstance(selectedCourse);
				gui.getOverviewCard().updateCourseList();
			}
			
		}
		
	}
	private void searchForStudent(ActionEvent e) {
		String name = JOptionPane.showInputDialog(null, "Name of the student:", "Student search", JOptionPane.QUESTION_MESSAGE);
		Student student = storage.getStudentByName(name);
		if(student != null) {
		setupStudentGradeCard(student, e);
		}
		else {
			JOptionPane.showMessageDialog(null, "Student " + name + " not found.");
		}
	}

	/**
	 * Saves grades from GradeCard to its respective student groups
	 */
	private void saveGradesByTable() {
		JTable gradeTable = gui.getGradeCard().getCourseTable();
		for(int i = 0; i < gradeTable.getRowCount(); i++) {
			
			Studentgroups group = (Studentgroups) gradeTable.getValueAt(i, 0);
			if(gradeTable.getValueAt(i, 1) instanceof Grade) {
				group.setGrade((Grade) gradeTable.getValueAt(i, 1));
			}
			else {
			Grade grade = Validators.gradeStringToGrade((String) gradeTable.getValueAt(i, 1));
			group.setGrade(grade);
			if(group.getGrade() != null) {
				daoFactory.getStudentGroupDAO().updateInstance(group);
			}
			}
			
		}
		
	}

	/**
	 * Saves a new student group
	 */
	private void saveNewGroup() {
		
		ArrayList<Student> markedStudents = gui.getgFrame().getMarkedStudents();
		
		Coursepart selectedPart = (Coursepart) gui.getGradeCard().getCoursePartBox().getSelectedItem();
		if(markedStudents.size() > 0 && markedStudents.size() <= selectedPart.getGroupSize()) {
			
			Studentgroups group = gui.getgFrame().getCurrentGroup();
			if(group == null ) {
			Studentgroups newGroup = new Studentgroups(markedStudents, selectedPart);
			daoFactory.getStudentGroupDAO().addnewInstance(newGroup);
			selectedPart.getGroups().add(newGroup);
			}
			else {
				group.setGroupMembers(markedStudents);
			}
			JOptionPane.showMessageDialog(null, "Group saved");
			gui.getgFrame().dispose();
			gui.setgFrame(null);
			editCourseTable();
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Invalid number of members selected. You chose " + markedStudents.size() + " students." + System.lineSeparator() +
			"Max students for this course part is " + selectedPart.getGroupSize());
		}
		
		
	}
	
	/**
	 * Sets up a new frame for group creation
	 * 
	 * @param editGroup,
	 *            whether or not the group is to be edit or a new one created
	 */
	private void setupGroupCreatorFrame(boolean editGroup) {

		Course selectedCourse = (Course) gui.getGradeCard().getCourseBox().getSelectedItem();

		Coursepart part = null;
		if (selectedCourse != null && gui.getGradeCard().getCoursePartBox().getSelectedItem() instanceof Coursepart) {
			
			part = (Coursepart) gui.getGradeCard().getCoursePartBox().getSelectedItem();
		}
		if (part != null) {

			ArrayList<Student> students = storage.getStudentsWithoutGroups(selectedCourse, part);
			if (students.size() == 0) {
				JOptionPane.showMessageDialog(null, "All students are assigned to a group on this course part");
			} else {
				if ((editGroup && gui.getGradeCard().getCourseTable().getSelectedRow() != -1) || !editGroup) {
					Groupframe frame = new Groupframe(this);
					gui.setgFrame(frame);
					DefaultTableModel model = (DefaultTableModel) frame.getMemberTable().getModel();

					model.setDataVector(GUIconverters.getStudentListForGroups(students),
							new String[] { "Student", "Member" });

					if (editGroup) {
						System.out.println(gui.getGradeCard().getCourseTable().getSelectedRow());
						Studentgroups group = (Studentgroups) gui.getGradeCard().getCourseTable()
								.getValueAt(gui.getGradeCard().getCourseTable().getSelectedRow(), 0);
						for (Student student : group.getGroupMembers()) {
							model.addRow(new Object[] { student, true });
							frame.setCurrentGroup(group);
						}

					}
					model.fireTableDataChanged();

				}
			}
		}

	}
	
	/**
	 * Save grades based on the input from the Studentgradecard
	 */
	private void saveGrades() {
		for (String gradeChange : gradeChanges) {
			if (gradeChange != null) {
				String[] changes = gradeChange.split("\\|");
				if(changes.length == 4) {
				Course course = storage.getCourseByName(changes[0]);
				if (course != null) {
					Coursepart part = course.getPartByDescription(changes[1]);
					if (part != null) {
						Studentgroups group = part.findGroupByStudent(storage.getStudentByName(changes[2]));
						if (group != null) {
							Grade grade = Grade.valueOf(changes[3]);
							if (grade != null) {
								group.setGrade(grade);
								daoFactory.getStudentGroupDAO().updateInstance(group);
							}
						}
					}
					}
				}

			}
		}
		gui.getStudentGradeCard().getInfoPanel().removeAll();
		gui.getStudentGradeCard().getInfoPanel().add(GUIconverters.updateStudentGradeCardInfoPanel((Student) gui.getStudentCard().getStudentlist().getSelectedValue()));
		gui.getStudentGradeCard().getInfoPanel().revalidate();
		gradeChanges.clear();
	}

	/**
	 * Updates the course part box of the grade card
	 */
	protected void updateCoursePartBox() {

		Course selectedCourse = (Course) gui.getGradeCard().getCourseBox().getSelectedItem();
		gui.getGradeCard().getCoursePartBox().removeAllItems();
		for(Coursepart part : selectedCourse.getParts()) {
			gui.getGradeCard().getCoursePartBox().addItem(part);
		}
		gui.getGradeCard().getCoursePartBox().addItem("View all students");
		
	}

	
	/**
	 * Updates the content of the grade table when a part is selected
	 */
	private void editCourseTable() {
		if (gui.getGradeCard().getCoursePartBox().getSelectedIndex() != -1) {
			if (gui.getGradeCard().getCoursePartBox().getSelectedItem().equals("View all students")) {
				gui.getGradeCard().updateCourseTable((Course) gui.getGradeCard().getCourseBox().getSelectedItem());
			} else {
				Coursepart selectedPart = (Coursepart) gui.getGradeCard().getCoursePartBox().getSelectedItem();
				gui.getGradeCard().updateCourseTable(selectedPart);
			}
		}

	}
	
	/**
	 * Refreshes the course table of the studentcard if the user wants to revert all changes
	 */
	private void undoStudentCourse() {
		gui.getStudentCard().updateTable();
	}

	/**
	 * Methods updates the course list of a given student based on which courses are marked.
	 * @return true if save is done
	 */
	private boolean saveStudentCourses() {
		Student student = (Student) gui.getStudentCard().getStudentlist().getSelectedValue();
		if (student != null) {
			ArrayList<Course> selectedCourses = gui.getStudentCard().getCheckedCourses(); // All marked courses

			ArrayList<Course> removingCourses = student.compareCourses(selectedCourses); // All courses the student was attending, but werent included in selectedCourses

			if (removingCourses.size() > 0) {
				String removingCourse = "All grades and assignements related to the student are removed upon course deletion"
						+ System.lineSeparator() + "Are you sure you want to remove the following courses: "
						+ System.lineSeparator();
				for (Course course : removingCourses) {
					removingCourse += course.getCourseName() + System.lineSeparator();
				}

				if (JOptionPane.showConfirmDialog(null, removingCourse, "Please confirm",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				}
				else {
					return false; // Ends method before any changes are made if the user chose "No".
				}
			}
			
			HashMap<Course, ArrayList<Studentgroups>> addedIDs = storage.updateCourses(student, selectedCourses);
			HashMap<Course, ArrayList<Studentgroups>> removedID = storage.removeCourses(student, removingCourses);
			updateStudentCourseDB(student, addedIDs, removedID);
			JOptionPane.showMessageDialog(null, "Courses saved");
			return true;
		}
		return false;
	}
	
	

	/**
	 * Updates the student courses which includes adding / removing courses in the DB and updating assignment groups
	 * @param student, the selected student
	 * @param newIds, map of new courses and groups
	 * @param removingCourses, map of courses to be removed
	 */
	private void updateStudentCourseDB(Student student, HashMap<Course, ArrayList<Studentgroups>> newIds, HashMap<Course, ArrayList<Studentgroups>> removingCourses) {
		for(Entry<Course, ArrayList<Studentgroups>> entry : newIds.entrySet()) {
			daoFactory.getStudentCourseDAO().addnewInstance(new Integer[] {student.getId(), entry.getKey().getCourseID()});
			for(Studentgroups group : entry.getValue()) {
				daoFactory.getStudentGroupDAO().addnewInstance(group);
			}
		}
		
		for(Entry<Course, ArrayList<Studentgroups>> entry : removingCourses.entrySet()) {
			daoFactory.getStudentCourseDAO().deleteInstance(new Integer[] {student.getId(), entry.getKey().getCourseID()});
			
			for(Studentgroups group : entry.getValue()) {
				if(group.getGroupMembers().size() == 1) {
					daoFactory.getStudentGroupDAO().deleteInstance(group);
				}
				else {
					daoFactory.getStudentGroupDAO().removeGroupMember(group, student);
				}
			}
			
		}
		
		
		
	}

	/**
	 * Methods deletes a student
	 */
	private void deleteStudent() {
		
		if(gui.getStudentCard().getStudentlist().getSelectedIndex() != -1) {
			Student student = (Student) gui.getStudentCard().getStudentlist().getSelectedValue();
			if(JOptionPane.showConfirmDialog(null, "Are you sure you want to delete student " + student.getName(), "Please confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			storage.getStudents().remove(student.getId());
				updateStudentCourseDB(student, new HashMap<>(), storage.removeCourses(student, student.getTakesCourse()));
				daoFactory.getStudentDAO().deleteInstance(student);
				gui.getStudentCard().updateStudentList();
				
			}
		}
		
	}
	
	/**
	 * Method saves a course based on the input from the Editcoursecard
	 */
	private void saveCourse() {
		String[] courseInput = { gui.getEditCard().getCourseName().getText(),
				gui.getEditCard().getCourseDescription().getText()};
		if(currentCourse != null) {
			if(!currentCourse.checkUpdates(courseInput)) {
				daoFactory.getCourseDAO().updateInstance(currentCourse);
			}
			
		}
		
		else {
		
			currentCourse = new Course(0, courseInput[0], courseInput[1]);
			int assignedID = daoFactory.getCourseDAO().addnewInstance(currentCourse);
			currentCourse.setCourseID(assignedID);
			storage.getCourses().put(currentCourse.getCourseID(), currentCourse);
		}

		int totalWeight = 0;
		String[][] partInput = gui.getEditCard().getPartInfo();
		for(int i = 0; i < partInput.length; i++) {
			totalWeight += Integer.valueOf(partInput[i][3]);
		}
		
		if(totalWeight != 100) {
			JOptionPane.showMessageDialog(null, "Grade weight got to be 100%");
		}
		else {
			savePart(partInput);
		}

		gui.getGradeCard().updateCourseBox();
		gui.getEditCard().refreshInfoPanel(currentCourse);
	}
	
	private void savePart(String[][] partInput) {
		
		for(int i = 0; i < partInput.length; i++) {
			Object[] selectedPartInput = Validators.convertCoursePartInput(partInput[i]);
			Coursepart currentPart = currentCourse.findPartByID((Integer) selectedPartInput[0]);
			
			if(currentPart == null) {
				currentPart = new Coursepart((Integer) selectedPartInput[0], currentCourse, (String) selectedPartInput[1], (Integer) selectedPartInput[2], (Integer) selectedPartInput[3]);
				int assignedID = daoFactory.getCoursepartDAO().addnewInstance(currentPart);
				currentPart.setId(assignedID);
				currentCourse.getParts().add(currentPart);
			}
			else {
				if(!currentPart.checkUpdates(selectedPartInput))  {
					daoFactory.getCoursepartDAO().updateInstance(currentPart);
				}
			}
		}
		
	}

	/**
	 * Methods creates a new sub part of the course in the edit coursee card
	 */
	private void createNewPart() {

		Object[] inputs = Validators.convertCoursePartInput(gui.getEditCard().getInputText(0));

		if (inputs != null) {
			if ((Integer) inputs[3] < 20) {
				JOptionPane.showMessageDialog(null, "Grade weight must be 20% or higher");
			} else {
				gui.getEditCard().setupInputPanel((Integer) inputs[0], (String) inputs[1], (Integer) inputs[2],
						(Integer) inputs[3]);
				updateCourseInput();
			}
		}

		else {
			JOptionPane.showMessageDialog(null, "Use numbers for group size and grade weight");
		}
	}
	
	/**
	 * Methods opens the edit course card with the selected course
	 * @param e
	 */
	private void openCourse(ActionEvent e) {
		JList<Object> courseList = gui.getOverviewCard().getCourseList();
		if(courseList.getSelectedIndex() != -1) {
		
			currentCourse = (Course) gui.getOverviewCard().getCourseList().getSelectedValue();
			
			gui.getEditCard().setupDefaultPartPanels();
			gui.getEditCard().getCourseName().setText(currentCourse.getCourseName());
			gui.getEditCard().getCourseDescription().setText(currentCourse.getDescription());
			for(Coursepart part : currentCourse.getParts()) {
				gui.getEditCard().setupInputPanel(part.getId(), part.getDescription(), part.getGroupSize(), part.getGradeWeight());
			}
			
			updateCourseInput();
			changeCard(e);
			
		}
	}
	
	/**
	 * Methods adds a new student into the collection of the name input is valid
	 */
	private void addStudent() {
		String studentName = JOptionPane.showInputDialog("Student name:");
		if(studentName != null && studentName.length() > 0) {

			Student student = new Student(0, studentName);
			int assignedID = daoFactory.getStudentDAO().addnewInstance(new Student(0, studentName));
			student.setId(assignedID);
			storage.getStudents().put(student.getId(), student);
			gui.getStudentCard().updateStudentList();
		}
		else {
			JOptionPane.showMessageDialog(null, "Invalid student name");
		}		
	}
	
	


	/**
	 * Methods setups up the course part panels of the edit card 
	 */
	protected void updateCourseInput() {

		gui.getEditCard().paintPartPanels();
		gui.getEditCard().refreshInfoPanel(currentCourse);

	}
	
	/**
	 * Methods is responsible of changing card of the cardholder in the main GUI and replacing the header content
	 * @param e
	 */
	private void changeCard(ActionEvent e) {

		JButton button = (JButton) e.getSource();

		gui.getHeader().setText(button.getActionCommand());
		CardLayout layout = (CardLayout) gui.getCardHolder().getLayout();
		layout.show(gui.getCardHolder(), button.getActionCommand());

		if (button.getActionCommand().equals(gui.getGradeCard().getHeader())) {
			if(0 < gui.getGradeCard().getCourseBox().getItemCount()) {
				gui.getGradeCard().getCourseBox().setSelectedIndex(0);
			}
			if (gui.getGradeCard().getCoursePartBox().getSelectedItem() instanceof Coursepart) {
				gui.getGradeCard()
						.updateCourseTable((Coursepart) gui.getGradeCard().getCoursePartBox().getSelectedItem());
			}
		}

	}

	/**
	 * @return the gui
	 */
	public GUI getGui() {
		return gui;
	}

	/**
	 * @param gui
	 *            the gui to set
	 */
	public void setGui(GUI gui) {
		this.gui = gui;
	}

	/**
	 * @return the storage
	 */
	public Storageunit getStorage() {
		return storage;
	}

	/**
	 * @param storage
	 *            the storage to set
	 */
	public void setStorage(Storageunit storage) {
		this.storage = storage;
	}

	/**
	 * @return the currentCourse
	 */
	public Course getCurrentCourse() {
		return currentCourse;
	}

	/**
	 * @param currentCourse
	 *            the currentCourse to set
	 */
	public void setCurrentCourse(Course currentCourse) {
		this.currentCourse = currentCourse;
	}

	/**
	 * @return the daoFactory
	 */
	public DAOfactory getDaoFactory() {
		return daoFactory;
	}

	/**
	 * @param daoFactory
	 *            the daoFactory to set
	 */
	public void setDaoFactory(DAOfactory daoFactory) {
		this.daoFactory = daoFactory;
	}


	/**
	 * @return the gradeChanges
	 */
	public ArrayList<String> getGradeChanges() {
		return gradeChanges;
	}

	/**
	 * @param gradeChanges the gradeChanges to set
	 */
	public void setGradeChanges(ArrayList<String> gradeChanges) {
		this.gradeChanges = gradeChanges;
	}

}

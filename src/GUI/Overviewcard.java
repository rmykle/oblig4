package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import data.Course;
import data.Coursepart;
import data.GUIconverters;
import data.Gradecalculator;
import data.Storageunit;

public class Overviewcard extends JPanel{
	
	private final String header = "Overview";
	private Listener listen;
	private JList<Object> courseList;
	private JTextField searchField;
	private JPanel courseInfo, courseButtons;
	private JButton openCourse, addCourse, addStudent, deleteStudent, deleteCourse;
	
	
	
	
	public Overviewcard(Listener listen) {
		this.listen = listen;
		initiate();
		
	}
	
	
	private void initiate() {
		setLayout(new GridLayout(1, 2));
		setupLeftCourseSide();
		setupRightCourseSide();
	}

	/**
	 * Sets up the right side of the course overview card
	 */
	private void setupRightCourseSide() {
		
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.setBackground(Color.white);
		rightPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.orange));
		JLabel courseHeader = new JLabel("Course information");
		courseHeader.setOpaque(true);
		courseHeader.setBackground(Color.orange);
		rightPanel.add(courseHeader, BorderLayout.NORTH);
		
		
		FlowLayout infoLayout = new FlowLayout();
		infoLayout.setAlignment(FlowLayout.LEFT);
		JPanel outerCourseInfo = new JPanel(infoLayout);
		outerCourseInfo.setBackground(Color.white);
		courseInfo = new JPanel();
		outerCourseInfo.add(courseInfo);
		rightPanel.add(outerCourseInfo, BorderLayout.CENTER);
		courseInfo.setBackground(Color.white);
		courseInfo.setLayout(new GridLayout(0, 1));
		
		JPanel fillerPanel = new JPanel();
		fillerPanel.setBackground(Color.orange);
		fillerPanel.setSize(courseButtons.getSize());
		fillerPanel.add(new JButton("test"));
		rightPanel.add(fillerPanel, BorderLayout.SOUTH);
		
		
		add(rightPanel);
		
	}

	/**
	 * Sets up the left side of the course overview card
	 */
	private void setupLeftCourseSide() {
		JPanel coursePanel = new JPanel(new BorderLayout());
		JLabel courseLabel = new JLabel("Courses");
		courseLabel.setBackground(Color.orange);
		courseLabel.setOpaque(true);
		coursePanel.add(courseLabel, BorderLayout.NORTH);
		
			Object[] courses = GUIconverters.getCourseNamesAsArray();
			courseList = new JList<>(courses);
			courseList.addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent e) {
				if(!e.getValueIsAdjusting()){
					courseInfo.removeAll(); // Removes existing labels
					if(courseList.getSelectedIndex() != -1) {
					HashMap<Coursepart, Integer[]> map = Gradecalculator.getCourseGradedInformation((Course) courseList.getSelectedValue());
					
					courseInfo.setLayout(new GridLayout(map.size()* 3,1));
					for(Entry<Coursepart, Integer[]> entry : map.entrySet()) {
						
						JLabel course = new JLabel(entry.getKey().getDescription());
						
						courseInfo.add(course);
						
						Integer[] value = entry.getValue();
						JLabel partInfo = new JLabel("Graded assignements: " + value[0]+ "%. Grades left: " + value[1] + ".");
						Font font = partInfo.getFont();
						partInfo.setFont(new Font("Arial", Font.PLAIN, 12));
						courseInfo.add(partInfo);
						
						courseInfo.add(new JLabel("")); // Filler
						
						courseInfo.revalidate();
						
					}
					}
					
				}
				}
			});
			coursePanel.add(courseList, BorderLayout.CENTER);
		
		 
		courseButtons = new JPanel(new FlowLayout());
		courseButtons.setBackground(Color.orange);
		openCourse = new JButton("Open Course");
		openCourse.addActionListener(listen);
		openCourse.setActionCommand("Course");
		addCourse = new JButton("Add Course");
		addCourse.setActionCommand("Course");
		addCourse.addActionListener(listen);
		deleteCourse = new JButton("Delete course");
		deleteCourse.addActionListener(listen);
		
		
		courseButtons.add(openCourse);
		courseButtons.add(addCourse);
		courseButtons.add(deleteCourse);
		coursePanel.add(courseButtons, BorderLayout.SOUTH);
		add(coursePanel);
		
		
	}
	
	/**
	 * Methods updates the course list in the course overview card
	 */
	protected void updateCourseList() {
		Object[] courses = GUIconverters.getCourseNamesAsArray();
		DefaultListModel<Object> model = new DefaultListModel<Object>();
		for(int i = 0; i < courses.length; i++) {
			model.addElement(courses[i]);
		}
		courseList.setModel(model);
	
	}

	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}
	/**
	 * @param listen the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}



	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}


	/**
	 * @return the searchField
	 */
	public JTextField getSearchField() {
		return searchField;
	}


	/**
	 * @param searchField the searchField to set
	 */
	public void setSearchField(JTextField searchField) {
		this.searchField = searchField;
	}


	/**
	 * @return the openCourse
	 */
	public JButton getOpenCourse() {
		return openCourse;
	}


	/**
	 * @param openCourse the openCourse to set
	 */
	public void setOpenCourse(JButton openCourse) {
		this.openCourse = openCourse;
	}


	/**
	 * @return the addCourse
	 */
	public JButton getAddCourse() {
		return addCourse;
	}


	/**
	 * @param addCourse the addCourse to set
	 */
	public void setAddCourse(JButton addCourse) {
		this.addCourse = addCourse;
	}

	/**
	 * @return the addStudent
	 */
	public JButton getAddStudent() {
		return addStudent;
	}


	/**
	 * @param addStudent the addStudent to set
	 */
	public void setAddStudent(JButton addStudent) {
		this.addStudent = addStudent;
	}


	/**
	 * @return the deleteStudent
	 */
	public JButton getDeleteStudent() {
		return deleteStudent;
	}


	/**
	 * @param deleteStudent the deleteStudent to set
	 */
	public void setDeleteStudent(JButton deleteStudent) {
		this.deleteStudent = deleteStudent;
	}


	/**
	 * @return the courseList
	 */
	public JList<Object> getCourseList() {
		return courseList;
	}


	/**
	 * @param courseList the courseList to set
	 */
	public void setCourseList(JList<Object> courseList) {
		this.courseList = courseList;
	}


	/**
	 * @return the deleteCourse
	 */
	public JButton getDeleteCourse() {
		return deleteCourse;
	}


	/**
	 * @param deleteCourse the deleteCourse to set
	 */
	public void setDeleteCourse(JButton deleteCourse) {
		this.deleteCourse = deleteCourse;
	}


	/**
	 * @return the courseInfo
	 */
	public JPanel getCourseInfo() {
		return courseInfo;
	}


	/**
	 * @param courseInfo the courseInfo to set
	 */
	public void setCourseInfo(JPanel courseInfo) {
		this.courseInfo = courseInfo;
	}
	

}

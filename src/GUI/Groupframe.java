package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import data.Student;
import data.Studentgroups;

/**
 * Class is responsible for creating a separate frame where the user can create and edit student groups
 * @author rmy002
 *
 */
public class Groupframe extends JFrame {
	
	private JSplitPane splitPanel;
	private JTable memberTable;
	private JButton closeButton, saveButton;
	private Studentgroups currentGroup;
	
	
	/**
	 * Constructor for the class Groupframe
	 * @param listen, listener for the card
	 */
	public Groupframe(Listener listen) {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Create group");
		setSize(600, 400);
		setResizable(false);
		
		setLayout(new BorderLayout()); 
		setLocationRelativeTo(null);
		
		JLabel header = new JLabel("Create new group");
		header.setBackground(Color.orange);
		header.setFont(new Font("Verdana", Font.BOLD, 20));
		header.setOpaque(true);
		add(header, BorderLayout.NORTH);
		
		Object[][] data = {};
		
		DefaultTableModel model = new DefaultTableModel(data, new String[] {"Student", "Member"}) {
			
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				
				switch (columnIndex) {
				case 1:
					  return getValueAt(0, columnIndex).getClass();

				default:
					  return getValueAt(0, columnIndex).getClass();
				}
			}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return column == 1;
			}
		};
		
		memberTable = new JTable(model);
		memberTable.getColumnModel().getColumn(1).setMaxWidth(100);
		memberTable.getColumnModel().getColumn(1).setMinWidth(100);
		JScrollPane memberScroll = new JScrollPane(memberTable);
		
		add(memberScroll, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel(new FlowLayout());
		saveButton = new JButton("Save");
		saveButton.addActionListener(listen);
		saveButton.setActionCommand("saveGroup");
		buttonPanel.add(saveButton);
		closeButton = new JButton("Close");
		closeButton.addActionListener(listen);
		closeButton.setActionCommand("closeGroupWindow");
		buttonPanel.add(closeButton);
		buttonPanel.setBackground(Color.orange);
		
		add(buttonPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	/**
	 * Methods gets all marked students in the student table
	 * @return all marked students as an arrayList
	 */
	protected ArrayList<Student> getMarkedStudents() {
		ArrayList<Student> markedStudents = new ArrayList<>();
		for(int i = 0; i < memberTable.getRowCount(); i++) {
			boolean mark = (boolean) memberTable.getValueAt(i, 1);
			if(mark == true) {
				Student markedStudent = (Student) memberTable.getValueAt(i, 0);
				markedStudents.add(markedStudent);
			}
			
		}
		return markedStudents;
		
	}

	/**
	 * @return the splitPanel
	 */
	public JSplitPane getSplitPanel() {
		return splitPanel;
	}

	/**
	 * @param splitPanel the splitPanel to set
	 */
	public void setSplitPanel(JSplitPane splitPanel) {
		this.splitPanel = splitPanel;
	}


	/**
	 * @return the memberTable
	 */
	public JTable getMemberTable() {
		return memberTable;
	}

	/**
	 * @param memberTable the memberTable to set
	 */
	public void setMemberTable(JTable memberTable) {
		this.memberTable = memberTable;
	}


	/**
	 * @return the closeButton
	 */
	public JButton getCloseButton() {
		return closeButton;
	}


	/**
	 * @param closeButton the closeButton to set
	 */
	public void setCloseButton(JButton closeButton) {
		this.closeButton = closeButton;
	}


	/**
	 * @return the saveButton
	 */
	public JButton getSaveButton() {
		return saveButton;
	}


	/**
	 * @param saveButton the saveButton to set
	 */
	public void setSaveButton(JButton saveButton) {
		this.saveButton = saveButton;
	}

	/**
	 * @return the currentGroup
	 */
	public Studentgroups getCurrentGroup() {
		return currentGroup;
	}

	/**
	 * @param currentGroup the currentGroup to set
	 */
	public void setCurrentGroup(Studentgroups currentGroup) {
		this.currentGroup = currentGroup;
	}

}

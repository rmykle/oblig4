package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Studentgradecard extends JPanel{
	
	private Listener listen;
	private final String header = "Grade student";
	private JLabel studentName;
	private JScrollPane courseScroll;
	private JPanel infoPanel;
	private JButton saveButton, cancelButton;
	
	public Studentgradecard(Listener listen) {
		this.listen = listen;
		
		initiate();
		
	}

	/**
	 * Sets up the studentgradecard
	 */
	private void initiate() {
		
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		studentName = new JLabel("");
		studentName.setOpaque(true);
		studentName.setBackground(Color.orange);
		add(studentName, BorderLayout.NORTH);
		
		courseScroll = new JScrollPane();
		add(courseScroll, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		saveButton = new JButton("Save");
		saveButton.addActionListener(listen);
		buttonPanel.add(saveButton);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(listen);
		cancelButton.setActionCommand("Students");
		buttonPanel.add(cancelButton);
		
		add(buttonPanel, BorderLayout.SOUTH);
		
		setupInfoPanel();
		
	}
	
	
	
	/**
	 * Sets up the info panel on the right side of the card
	 */
	private void setupInfoPanel() {
		
		infoPanel = new JPanel();
		infoPanel.setPreferredSize(new Dimension(250, 400));
		JScrollPane infoScroll = new JScrollPane(infoPanel);
		add(infoScroll, BorderLayout.EAST);
		infoPanel.setBackground(Color.white);
		
	}

	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @return the studentName
	 */
	public JLabel getStudentName() {
		return studentName;
	}

	/**
	 * @param studentName the studentName to set
	 */
	public void setStudentName(JLabel studentName) {
		this.studentName = studentName;
	}

	/**
	 * @return the courseScroll
	 */
	public JScrollPane getCourseScroll() {
		return courseScroll;
	}

	/**
	 * @param courseScroll the courseScroll to set
	 */
	public void setCourseScroll(JScrollPane courseScroll) {
		this.courseScroll = courseScroll;
	}

	/**
	 * @return the saveButton
	 */
	public JButton getSaveButton() {
		return saveButton;
	}

	/**
	 * @param saveButton the saveButton to set
	 */
	public void setSaveButton(JButton saveButton) {
		this.saveButton = saveButton;
	}

	/**
	 * @return the cancelButton
	 */
	public JButton getCancelButton() {
		return cancelButton;
	}

	/**
	 * @param cancelButton the cancelButton to set
	 */
	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	/**
	 * @return the infoPanel
	 */
	public JPanel getInfoPanel() {
		return infoPanel;
	}

	/**
	 * @param infoPanel the infoPanel to set
	 */
	public void setInfoPanel(JPanel infoPanel) {
		this.infoPanel = infoPanel;
	}

}

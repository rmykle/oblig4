package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import data.Course;
import data.Coursepart;
import data.GUIconverters;

/**
 * Class is responsible for creating a card where the user is able to select a course/coursepart and grade groups in a table. The card also enables the user to 
 * create new student groups.
 * @author rmy002
 *
 */
public class Gradecard extends JPanel {
	
	private final String header = "Grades";
	private Listener listen;
	private JComboBox<Object> courseBox, coursePartBox;
	private JButton saveCourses, newGroup, editGroup;
	private JTable courseTable; 
	private JScrollPane tableScroll;

	/**
	 * Constructor for the class Gradecard
	 * @param listen, listener for the card
	 */
	public Gradecard(Listener listen) {
		this.listen = listen;
		
		initiate();
	}
	
	

	/**
	 * Sets up the card panel and its core components
	 */
	private void initiate() {
		setLayout(new BorderLayout());
		
		JPanel courseChoosePanel = new JPanel(new FlowLayout());
		add(courseChoosePanel, BorderLayout.NORTH);
		courseChoosePanel.setBackground(Color.orange);
		
		courseChoosePanel.add(new JLabel("Course"));
		courseBox = new JComboBox<>(listen.getStorage().getCourses().values().toArray());
		courseBox.setPreferredSize(new Dimension(150, 20));
		courseBox.addActionListener(listen);
		courseChoosePanel.add(courseBox);
		
		courseChoosePanel.add(new JLabel("Course part:"));
		coursePartBox = new JComboBox<>();
		coursePartBox.addActionListener(listen);
		coursePartBox.setPreferredSize(new Dimension(150, 20));
		courseChoosePanel.add(coursePartBox);
		
		
		courseTable = new JTable();
		courseTable.setOpaque(true);
		courseTable.setFillsViewportHeight(true);
		courseTable.setBackground(Color.white);
		
		tableScroll = new JScrollPane(courseTable);
		add(tableScroll, BorderLayout.CENTER);
	
		JPanel courseButtons = new JPanel(new FlowLayout());
		courseButtons.setBackground(Color.orange);
		
		newGroup = new JButton("Create Group");
		newGroup.addActionListener(listen);
		courseButtons.add(newGroup);
		
		editGroup = new JButton("Edit group");
		editGroup.addActionListener(listen);
		courseButtons.add(editGroup);
		
		saveCourses = new JButton("Save changes");
		saveCourses.addActionListener(listen);
		courseButtons.add(saveCourses);
		add(courseButtons, BorderLayout.SOUTH);
		
	}
	
	/**
	 * Methods updates the course box in the grade card
	 */
	protected void updateCourseBox() {
		DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<>(listen.getStorage().getCourses().values().toArray());
		this.courseBox.setModel(model);
	}
	/**
	 * Methods updates the grade card table showing student groups of a given course part
	 * @param coursePart, the selected course part
	 */
	protected void updateCourseTable(Object coursePart) {
		
		remove(tableScroll);
		
		courseTable = GUIconverters.getCourseGradeTable(coursePart);
		tableScroll = new JScrollPane(courseTable);
		
		add(tableScroll, BorderLayout.CENTER);
		
		revalidate();
		repaint();
	}



	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}


	/**
	 * @param listen the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}


	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}



	/**
	 * @return the courseTable
	 */
	public JTable getCourseTable() {
		return courseTable;
	}




	/**
	 * @param courseTable the courseTable to set
	 */
	public void setCourseTable(JTable courseTable) {
		this.courseTable = courseTable;
	}




	/**
	 * @return the saveCourses
	 */
	public JButton getSaveCourses() {
		return saveCourses;
	}




	/**
	 * @param saveCourses the saveCourses to set
	 */
	public void setSaveCourses(JButton saveCourses) {
		this.saveCourses = saveCourses;
	}




	/**
	 * @return the newGroup
	 */
	public JButton getNewGroup() {
		return newGroup;
	}




	/**
	 * @param newGroup the newGroup to set
	 */
	public void setNewGroup(JButton newGroup) {
		this.newGroup = newGroup;
	}




	/**
	 * @return the tableScroll
	 */
	public JScrollPane getTableScroll() {
		return tableScroll;
	}




	/**
	 * @param tableScroll the tableScroll to set
	 */
	public void setTableScroll(JScrollPane tableScroll) {
		this.tableScroll = tableScroll;
	}




	/**
	 * @return the courseBox
	 */
	public JComboBox<Object> getCourseBox() {
		return courseBox;
	}




	/**
	 * @param courseBox the courseBox to set
	 */
	public void setCourseBox(JComboBox<Object> courseBox) {
		this.courseBox = courseBox;
	}




	/**
	 * @return the coursePartBox
	 */
	public JComboBox<Object> getCoursePartBox() {
		return coursePartBox;
	}




	/**
	 * @param coursePartBox the coursePartBox to set
	 */
	public void setCoursePartBox(JComboBox<Object> coursePartBox) {
		this.coursePartBox = coursePartBox;
	}




	/**
	 * @return the editGroup
	 */
	public JButton getEditGroup() {
		return editGroup;
	}




	/**
	 * @param editGroup the editGroup to set
	 */
	public void setEditGroup(JButton editGroup) {
		this.editGroup = editGroup;
	}






	
	
}

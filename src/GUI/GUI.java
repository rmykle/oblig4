package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.mysql.jdbc.Connection;

import sqlhandling.SqlDAOFactory;

/**
 * The  class GUI is the core component in the GUI, holding the menu, header and the cardHolder for the program content.
 * @author rmy002
 *
 */
public class GUI extends JFrame {
	
	private static GUI gui = null;
	private JLabel header;
	private JPanel cardHolder;
	private Editcoursecard editCard;
	private Overviewcard overviewCard;
	private Studentcard studentCard;
	private Gradecard gradeCard;
	private Listener listen;
	private Studentgradecard studentGradeCard;
	private Groupframe gFrame;

	
	private GUI(Listener listen) {
		this.listen = listen;
		
		init();
	}
	
	/**
	 * Sets up the frame and the core cards
	 */
	private void init() {
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
				try {
					connection.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				super.windowClosing(e);
			}
			
		});
			
		setSize(1000, 800);	
		setTitle("No Country For Old Grades");
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		
		
		
		cardHolder = new JPanel(new CardLayout());
		cardHolder.setBackground(Color.green);
		add(cardHolder, BorderLayout.CENTER);
		
		overviewCard = new Overviewcard(listen);
		cardHolder.add(overviewCard, overviewCard.getHeader());
		
		gradeCard = new Gradecard(listen);
		cardHolder.add(gradeCard, gradeCard.getHeader());
		
		studentCard = new Studentcard(listen);
		cardHolder.add(studentCard, studentCard.getHeader());
		
		studentGradeCard = new Studentgradecard(listen);
		cardHolder.add(studentGradeCard, studentGradeCard.getHeader());
		
		editCard = new Editcoursecard(listen);
		cardHolder.add(editCard, editCard.getHeader());
		
		initHeader();
		
		
		setVisible(true);
	}
	
	/**
	 * Sets up the GUI header
	 */
	private void initHeader() {
		
		JPanel upperPanel = new JPanel();
		upperPanel.setLayout(new BorderLayout());
		upperPanel.setBackground(Color.red);
		add(upperPanel, BorderLayout.NORTH);
		
		initMenu(upperPanel);
		
		
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.LEFT);
		JPanel headerPanel = new JPanel();
		headerPanel.setBackground(Color.white);
		headerPanel.setLayout(layout);
		upperPanel.add(headerPanel, BorderLayout.SOUTH);
		header = new JLabel(overviewCard.getHeader());
		header.setFont(new Font("Verdana", Font.BOLD, 20));
		headerPanel.add(header);
		
		
	}

	/**
	 * Creates the GUI main menu
	 * @param upperPanel, where the menu is placed
	 */
	private void initMenu(JPanel upperPanel) {
		
		JPanel menuPanel = new JPanel();
		menuPanel.setLayout(new GridLayout(1, 4));
		menuPanel.setPreferredSize(new Dimension(Integer.MAX_VALUE, 100));
		upperPanel.add(menuPanel, BorderLayout.NORTH);
		menuPanel.setBackground(Color.orange);
		
		JButton courseButton = new JButton("Courses");
		courseButton.addActionListener(listen);
		courseButton.setActionCommand(overviewCard.getHeader());
		menuPanel.add(courseButton);
		courseButton.setBackground(Color.orange);
		
		JButton gradeButton = new JButton("Grades");
		gradeButton.addActionListener(listen);
		gradeButton.setActionCommand(gradeCard.getHeader());
		gradeButton.setBackground(Color.orange);
		menuPanel.add(gradeButton);
		
		JButton studentButton = new JButton("Students");
		studentButton.addActionListener(listen);
		studentButton.setActionCommand(studentCard.getHeader());
		studentButton.setBackground(Color.orange);
		
		menuPanel.add(studentButton);
		
	}
	

	/**
	 * @return the gui
	 */
	public static GUI getGui(Listener listen) {
		if(gui == null)  gui = new GUI(listen);
		return gui;
	}

	/**
	 * @return the header
	 */
	public JLabel getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(JLabel header) {
		this.header = header;
	}

	/**
	 * @return the cardHolder
	 */
	public JPanel getCardHolder() {
		return cardHolder;
	}

	/**
	 * @param cardHolder the cardHolder to set
	 */
	public void setCardHolder(JPanel cardHolder) {
		this.cardHolder = cardHolder;
	}

	/**
	 * @return the editCard
	 */
	public Editcoursecard getEditCard() {
		return editCard;
	}

	/**
	 * @param editCard the editCard to set
	 */
	public void setEditCard(Editcoursecard editCard) {
		this.editCard = editCard;
	}

	/**
	 * @param gui the gui to set
	 */
	public static void setGui(GUI gui) {
		GUI.gui = gui;
	}

	/**
	 * @return the listen
	 */
	public Listener getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(Listener listen) {
		this.listen = listen;
	}


	/**
	 * @return the gui
	 */
	public static GUI getGui() {
		return gui;
	}

	/**
	 * @return the studentCard
	 */
	public Studentcard getStudentCard() {
		return studentCard;
	}

	/**
	 * @param studentCard the studentCard to set
	 */
	public void setStudentCard(Studentcard studentCard) {
		this.studentCard = studentCard;
	}

	/**
	 * @return the overviewCard
	 */
	public Overviewcard getOverviewCard() {
		return overviewCard;
	}

	/**
	 * @param overviewCard the overviewCard to set
	 */
	public void setOverviewCard(Overviewcard overviewCard) {
		this.overviewCard = overviewCard;
	}

	/**
	 * @return the gradeCard
	 */
	public Gradecard getGradeCard() {
		return gradeCard;
	}

	/**
	 * @param gradeCard the gradeCard to set
	 */
	public void setGradeCard(Gradecard gradeCard) {
		this.gradeCard = gradeCard;
	}

	/**
	 * @return the studentGradeCard
	 */
	public Studentgradecard getStudentGradeCard() {
		return studentGradeCard;
	}

	/**
	 * @param studentGradeCard the studentGradeCard to set
	 */
	public void setStudentGradeCard(Studentgradecard studentGradeCard) {
		this.studentGradeCard = studentGradeCard;
	}

	/**
	 * @return the gFrame
	 */
	public Groupframe getgFrame() {
		return gFrame;
	}

	/**
	 * @param gFrame the gFrame to set
	 */
	public void setgFrame(Groupframe gFrame) {
		this.gFrame = gFrame;
	}
}




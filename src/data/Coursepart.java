package data;

import java.util.ArrayList;

/**
 * Class represents a sub-part of a course
 * @author rmy002
 *
 */
public class Coursepart {
	
	private int id;
	private Course partOf;
	private String description;
	private int groupSize;
	private int gradeWeight;
	private ArrayList<Studentgroups> groups;
	

	/**
	 * Constructor for the class Coursepart
	 * @param id - id of the part
	 * @param description - description of the part
	 * @param groupSize - maximum size of the group size
	 * @param gradeWeight - how much the part grade influences the total course grade
	 */
	public Coursepart(int id, Course course, String description, int groupSize, int gradeWeight) {
		this.id = id;
		this.partOf = course;
		this.description = description;
		this.groupSize = groupSize;
		this.gradeWeight = gradeWeight;
		this.groups = new ArrayList<>();
	}
	
	
	/**
	 * Checks for update given new course part information;
	 * @param inputs, new course part information
	 * @return true of no changes are made
	 */
	public boolean checkUpdates(Object[] inputs) {
		if(!this.description.equals((String) inputs[1]) || this.groupSize != (Integer) inputs[2] || this.gradeWeight != (Integer) inputs[3]){
			this.description = (String) inputs[1];
			this.groupSize = (Integer) inputs[2];
			this.gradeWeight = (Integer) inputs[3];
			return false;
		}
		return true;
	}
	
	/**
	 * Find studentgroup of the given student
	 * @param student selected
	 * @return the studentgroup
	 */
	public Studentgroups findGroupByStudent(Student student) {
		for(Studentgroups group : groups) {
			if(group.getGroupMembers().contains(student)) {
				return group;
			}
		}
		return null;
	}
	
	/**
	 * Find student group by id
	 * @param id of the group
	 * @return the group found
	 */
	public Studentgroups findGroupByID(int id) {
		for(Studentgroups group : groups) {
			if(group.getId() == id) return group;
		}
		return null;
	}
	
	/**
	 * toString of the Coursepart object
	 */
	@Override
	public String toString() {
		return description;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}




	/**
	 * @return the groupSize
	 */
	public int getGroupSize() {
		return groupSize;
	}




	/**
	 * @param groupSize the groupSize to set
	 */
	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}




	/**
	 * @return the gradeWeight
	 */
	public int getGradeWeight() {
		return gradeWeight;
	}




	/**
	 * @param gradeWeight the gradeWeight to set
	 */
	public void setGradeWeight(int gradeWeight) {
		this.gradeWeight = gradeWeight;
	}


	/**
	 * @return the groups
	 */
	public ArrayList<Studentgroups> getGroups() {
		return groups;
	}


	/**
	 * @param groups the groups to set
	 */
	public void setGroups(ArrayList<Studentgroups> groups) {
		this.groups = groups;
	}


	/**
	 * @return the partOf
	 */
	public Course getPartOf() {
		return partOf;
	}


	/**
	 * @param partOf the partOf to set
	 */
	public void setPartOf(Course partOf) {
		this.partOf = partOf;
	}

}

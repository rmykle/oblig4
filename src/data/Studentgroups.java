package data;

import java.util.ArrayList;

import enums.Grade;

/**
 * Class represents a group of student with an assignment
 * @author rmy002
 *
 */
public class Studentgroups {
	
	private int id;
	private Coursepart part;
	private ArrayList<Student> groupMembers;
	private Grade grade;
	
	/**
	 * Constructor for the class Studentgroups
	 * @param students of the new group
	 */
	public Studentgroups(ArrayList<Student> students, Coursepart part) {
		this.groupMembers = students;
		this.part = part;
	}
	
	
	/**
	 * toString of the StudentGroup object returning all group members
	 */
	@Override
	public String toString() {
		String members = "";
		for(Student student : groupMembers) {
			members += student.getName() + "(ID: " + student.getId()+") -";
		}
		return members;
	}
	

	/**
	 * @return the groupMembers
	 */
	public ArrayList<Student> getGroupMembers() {
		return groupMembers;
	}

	/**
	 * @param groupMembers the groupMembers to set
	 */
	public void setGroupMembers(ArrayList<Student> groupMembers) {
		this.groupMembers = groupMembers;
	}



	/**
	 * @return the grade
	 */
	public Grade getGrade() {
		return grade;
	}



	/**
	 * @param grade the grade to set
	 */
	public void setGrade(Grade grade) {
		this.grade = grade;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the part
	 */
	public Coursepart getPart() {
		return part;
	}


	/**
	 * @param part the part to set
	 */
	public void setPart(Coursepart part) {
		this.part = part;
	}
	
	

}

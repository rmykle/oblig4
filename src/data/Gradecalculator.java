package data;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.HashMap;

import enums.Grade;

/**
 * Class gradecalculator is responsible for calculating average grades of a student
 * @author rmy002
 *
 */
public abstract class Gradecalculator {
	
	/**
	 * Methods returns an average grade based on all courses
	 * @param student
	 * @return the average grade
	 */
	public static Grade getTotalAverage(Student student) {
		int TotalGradeAsInt = 0;
		int groups = 0;
		for(Course course : student.getTakesCourse()) {
			for(Coursepart part : course.getParts()) {
				for(Studentgroups group : part.getGroups()) {
					if(group.getGroupMembers().contains(student)) {
						if(group.getGrade() == null) {
							return convertIntToGrade(0);
						}
						else {
							TotalGradeAsInt += group.getGrade().getGradeAsInt();
							groups++;
						}
					}
				}
			}
		}
		
		return convertIntToGrade(TotalGradeAsInt / groups);
		
	}
	
	/**
	 * Methods returns a grade for a spesific course and the grades of its sub parts
	 * @param student
	 * @param course
	 * @return
	 */
	public static int getAverageGrade(Student student, Course course) {
		
		ArrayList<Double> values = new ArrayList<>();
		for(Coursepart part : course.getParts()) {
			for(Studentgroups group : part.getGroups()) {
				if(group.getGroupMembers().contains(student)) {
					if(group.getGrade() != null) {
					
						double partWeight = part.getGradeWeight() * 0.01;
						values.add(partWeight * group.getGrade().getGradeAsInt());
					}
					else {
						return 0;
					}
				}
			}
		}
		if(values.size() < course.getParts().size()) {
			return 0;
		}
		double sum = 0;
		for(int i = 0; i < values.size(); i++) {
			sum += values.get(i);
		}
		
		return (int) sum;
	}
	
	/**
	 * Converts the average grade from an int value to Grade
	 * @param gradeInt
	 * @return the grade
	 */
	public static Grade convertIntToGrade(int gradeInt) {
		switch (gradeInt) {
		case 1:
			return Grade.F;
		case 2:
			return Grade.E;
		case 3: 
			return Grade.D;
		case 4:
			return Grade.C;
		case 5:
			return Grade.B;
		case 6:
			return Grade.A;
		default:
			return Grade.NOT_GRADED;
		}
	}
	
	/**
	 * Gets information about the current status of the grading in course and its sub-parts
	 * @param course in question
	 * @return 
	 */
	public static HashMap<Coursepart, Integer[]> getCourseGradedInformation(Course course) {
		HashMap<Coursepart, Integer[]> parts = new HashMap<>();
		for(Coursepart part : course.getParts()) {
			System.out.println("NYDEL");
			
			int gradedCount = 0;
			for(Studentgroups group : part.getGroups()) {
				if(group.getGrade() != null) {
					gradedCount++;
				}
			}
			if(part.getGroups().size()> 0) {
			int gradePercent = (int)((gradedCount * 100.0f) / part.getGroups().size());
			Integer[] percent = { gradePercent, part.getGroups().size()-gradedCount };
			parts.put(part, percent);
			}
			else {
				parts.put(part, new Integer[] {gradedCount, part.getGroups().size()});
			}
		}
		return parts;
	}
}

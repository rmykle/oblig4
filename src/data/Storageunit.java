package data;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import enums.Grade;

/**
 * Class works as a storage facility for courses and students in the program, including various methods to maintain the data inserted
 * @author rmy002
 *
 */
public class Storageunit {
	
	private static Storageunit storage = null;
	private static HashMap<Integer, Course> courses = new HashMap<>();
	private static HashMap<Integer, Student> students = new HashMap<>();
	
	private Storageunit() {
	}
	
	

	
	/**
	 * Methods adds a course to a student and creates solo groups for all course parts with just one member
	 * @param student, the selected student
	 * @param course, the selected course
	 * @return true if course added
	 */
	public boolean addStudentToCourse(Student student, Course course) {
		if(!student.getTakesCourse().contains(course)) {
			student.getTakesCourse().add(course);
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Creates solo assignments when a student is signed up for a course
	 * @param student, the selected student
	 * @param course, the selected course
	 * @return an arraylist of the new groups created
	 */
	public ArrayList<Studentgroups> addStudentToIndividualGroups(Student student, Course course) {
		ArrayList<Studentgroups> groups = new ArrayList<>();
					
			for(Coursepart part : course.getParts()) {
				if(part.getGroupSize() == 1) {
					ArrayList<Student> studentList = new ArrayList<>();
					studentList.add(student);
					Studentgroups group = new Studentgroups(studentList, part);
					part.getGroups().add(group);
					groups.add(group);
				}
			}
			
		
		return groups;
	}
	
	/**
	 * Finds coursepart based on ID
	 * @param id of the part
	 * @return the part found
	 */
	public Coursepart getCoursePartByID(int id) {
		for(Course course : courses.values()) {
			for(Coursepart part : course.getParts()) {
				if(part.getId() == id) return part;
			}
		}
		return null;
		
	}
	

	/**
	 * Methods updates the course list of a given student. New courses are added and old courses are removed. 
	 * Any any assignments of the given course are removed in the proccess.
	 * @param removeCourses 
	 * @param student, the selected student
	 * @param selectedCourses, the selected courses
	 * @return arraylist of the ids of added courses
	 */
	public HashMap<Course, ArrayList<Studentgroups>> updateCourses(Student student, ArrayList<Course> selectedCourses) {

		HashMap<Course, ArrayList<Studentgroups>> addedCourses = new HashMap<>();// ID's
																					// for
		for (Course course : selectedCourses) {
			if(addStudentToCourse(student, course)) {

			ArrayList<Studentgroups> groups = addStudentToIndividualGroups(student, course);
			addedCourses.put(course, groups);
			}

		}
		return addedCourses;

	}

	public HashMap<Course, ArrayList<Studentgroups>> removeCourses(Student student, ArrayList<Course> removedCourses) {
		
		HashMap<Course, ArrayList<Studentgroups>> removedMap = new HashMap<>();

		Iterator<Course> it = student.getTakesCourse().iterator();
		while (it.hasNext()) {
			Course course = it.next();
			ArrayList<Studentgroups> groups = new ArrayList<>();
			
			if (removedCourses.contains(course)) {
				for (Coursepart part : course.getParts()) {
					
					for (Studentgroups group : part.getGroups()) {
						if (group.getGroupMembers().contains(student)) {
							groups.add(group);

							if (group.getGroupMembers().size() > 1) {
								group.getGroupMembers().remove(student);
								break;
							} else {
								part.getGroups().remove(group);
								break;
							}
						}
					}
				}
				
				removedMap.put(course, groups);
				it.remove();

			}
		}
		return removedMap;

	}
	
	
	public void deleteCourse(Course selectedCourse) {
		courses.remove(selectedCourse.getCourseID());
		for(Student student : students.values()) {
			if(student.getTakesCourse().contains(selectedCourse)) {
				student.getTakesCourse().remove(selectedCourse);
			}
		}
		
	}
	
	
	
	/**
	 * Methods finds all students whom are attending a given course
	 * @param course, the selected course
	 * @return an arraylist of all students of the course
	 */
	public ArrayList<Student> getStudentsByCourse(Course course) {
		ArrayList<Student> students = new ArrayList<>();
		
		for(Student student : this.students.values()) {
			if(student.getTakesCourse().contains(course)) {
				students.add(student);
			}
		}
		return students;
	}
	
	/**
	 * Methods gets all students without a group assigned to the given course part
	 * @param course, the selected course
	 * @param part. the selected course part
	 * @return an arraylist of all members for the course without a group for the given part
	 */
	public ArrayList<Student> getStudentsWithoutGroups(Course course, Coursepart part) {
		ArrayList<Student> students = new ArrayList<>();
		
		for(Student student : getStudentsByCourse(course)) {
			
				if(part.getGroups().size() == 0) {
					students.add(student);
				}
				else {
					
				
				for(int i = 0; i < part.getGroups().size(); i++) {
					
					if(part.getGroups().get(i).getGroupMembers().contains(student)) {
						break;
					}
					else {
						if(i == part.getGroups().size() -1 && !part.getGroups().get(i).getGroupMembers().contains(student)) {
							students.add(student);
						}
					}
					
				}
				}
			}
			
		
		
		return students;
	}
	
	/**
	 * Finds a course by its name
	 * @param name of the course
	 * @return the course found
	 */
	public Course getCourseByName(String name) {
		
		for(Course course : courses.values()) {
			if(course.getCourseName().equals(name)) return course;
		}
		
		return null;
	}
	
	/**
	 * Finds student by his or hers name
	 * @param name of the student
	 * @return the student found
	 */
	public Student getStudentByName(String name) {
		for(Student student : students.values()) {
			if(student.getName().equals(name)) return student;
		}
		return null;
	}

	
	
	/**
	 * @return the courses
	 */
	public HashMap<Integer, Course> getCourses() {
		return courses;
	}

	/**
	 * @param courses the courses to set
	 */
	public void setCourses(HashMap<Integer, Course> courses) {
		this.courses = courses;
	}

	/**
	 * @return the storage
	 */
	public static Storageunit getStorage() {
		if(storage == null) storage = new Storageunit();
		return storage;
	}

	/**
	 * @param storage the storage to set
	 */
	public static void setStorage(Storageunit storage) {
		Storageunit.storage = storage;
	}

	/**
	 * @return the students
	 */
	public HashMap<Integer, Student> getStudents() {
		return students;
	}

	/**
	 * @param students the students to set
	 */
	public void setStudents(HashMap<Integer, Student> students) {
		Storageunit.students = students;
	}




	

}

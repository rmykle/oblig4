package data;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import GUI.Listener;
import enums.Grade;
import utilities.Validators;

/**
 * Class GUIconverters is responsible for creating GUI friendly elements such as arrays based on data from elsewhere in the program.
 * @author rmy002
 *
 */
public abstract class GUIconverters {
	
	/**
	 * Creates a 2D array of all courses for the Studentcard. Courses the student is already taking will be marked by default.
	 * @param selectedStudent, the chosen one
	 * @return all courses as a 2D array consisting of {Course, Boolean}
	 */
	public static Object[][] getStudentCardCourseOverview(Student selectedStudent) {
		Storageunit storage = Storageunit.getStorage();

		Map<Course, Boolean> courseMap = new HashMap<>();
		for (Course course : storage.getCourses().values()) {
			courseMap.put(course, false);
		}
		if (selectedStudent.getTakesCourse().size() > 0) {
			for (Course course : selectedStudent.getTakesCourse()) {
				courseMap.put(course, true);
			}
		}

		Object[][] courses = new Object[storage.getCourses().size()][];
		int index = 0;
		for (Entry<Course, Boolean> course : courseMap.entrySet()) {
			courses[index] = new Object[] { course.getKey(), course.getValue() };
			index++;
		}

		return courses;

	}

	
	
	
	
	/**
	 * Method gets all students as an array
	 * @return all students as array
	 */
	public static Object[] getStudentAsArray() {
		return Storageunit.getStorage().getStudents().values().toArray();

	}
	
	/**
	 * Gets all courses as an array
	 * @param coursesMap
	 * @return courses as array
	 */
	public static Object[] getCourseNamesAsArray() {
		
		return Storageunit.getStorage().getCourses().values().toArray();
		
	}
	
	/**
	 * Gets a table containing all groups of a given course part and the related grade
	 * @param coursepart, the part selected
	 * @return the new table
	 */
	public static JTable getCourseGradeTable(Object course) {
		DefaultTableModel model = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex){
				return columnIndex == 1; 
				}
		};
		
		if(course instanceof Coursepart) {
		Object[] columns = { "Group", ((Coursepart) course).getDescription() + " ("+((Coursepart) course).getGradeWeight()+"%)"};
		
		Object[][] tableData = getGradeCardPartGroups((Coursepart) course);
		
		model.setDataVector(tableData, columns);
		}
		
		else if(course instanceof Course) {
			Object[] columns = {"Student" , "Final grade"};
			Object[][] tableData = getFinalGradeData((Course) course);
			model.setDataVector(tableData, columns);
		}
		
		JTable newTable = new JTable(model);
		newTable.getColumnModel().getColumn(1).setMinWidth(200);
		newTable.getColumnModel().getColumn(1).setMaxWidth(200);
		
		newTable.setOpaque(true);
		newTable.setFillsViewportHeight(true);
		newTable.setBackground(Color.white);
		newTable.setSelectionBackground(Color.orange);
		
		return newTable;
	}
	
	private static Object[][] getFinalGradeData(Course course) {
		Storageunit storage = Storageunit.getStorage();
		
		List<Student> streamResult = storage.getStudents().values().stream().filter(s -> s.getTakesCourse().contains(course)).collect(Collectors.toList());
		Object[][] tableData = new Object[streamResult.size()][2];
		for(int i = 0; i < tableData.length; i++) {
			tableData[i][0] = streamResult.get(i);
			tableData[i][1] = Gradecalculator.convertIntToGrade(Gradecalculator.getAverageGrade(streamResult.get(i), course)).getGradeAsString();
		}
		
		
		
		return tableData;
	}





	/**
	 * Sets up the table data for the grade card table
	 * @param coursepart, the course part selected
	 * @return the table data as 2D array
	 */
	private static Object[][] getGradeCardPartGroups(Coursepart coursepart) {
		int partGroups = coursepart.getGroups().size();
		Object[][] tableData = new Object[partGroups][2];
		for(int i = 0; i < coursepart.getGroups().size(); i++) {
			Studentgroups group = coursepart.getGroups().get(i);
			Object[] groupArray = new Object[2];
			
			groupArray[0] = group;
			if(group.getGrade() != null) {
			groupArray[1] = group.getGrade();
			}
			tableData[i] = groupArray;
		}
		return tableData;
	}

	/**
	 * Metod creates a 2D array of students, where a boolean value is added,  representing whether or not the student is to be added to the group
	 * @param students, the students of the course without a group
	 * @return an array of the students
	 */
	public static Object[][] getStudentListForGroups(ArrayList<Student> students) {
		Object[][] studentArray = new Object[students.size()][2];
		for(int i = 0; i < studentArray.length; i++) {
			studentArray[i][0] = students.get(i);
			studentArray[i][1] = false;
		}
		
		return studentArray;
	}
	
	/**
	 * Methods sets up the highest level panel of the student grade card
	 * @param student, the chosen student
	 * @param listen, listener for the panels
	 * @return the new content panel to be used in Studentgradecard
	 */
	public static JPanel setupCoursePanel(Student student, Listener listen) {
		
		JPanel mainCoursePanel = new JPanel();
		mainCoursePanel.setBackground(Color.white);
		mainCoursePanel.setLayout(new BoxLayout(mainCoursePanel, BoxLayout.PAGE_AXIS));
		
		for(Course course : student.getTakesCourse()) {
			
			mainCoursePanel.add(getCoursePanel(course, student, listen));
			mainCoursePanel.add(Box.createRigidArea(new Dimension(0, 20)));

		}
		
		return mainCoursePanel;
		
	}
	
	/**
	 * Sets up a new panel for each course the student is attending
	 * @param course, the chosen course
	 * @param student, the chosen student
	 * @param listen, listener for the panels
	 * @return the new course panel
	 */
	private static JPanel getCoursePanel(Course course, Student student, Listener listen) {
		
		JPanel subCoursePanel = new JPanel(new BorderLayout());
		subCoursePanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.orange));
		subCoursePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 130*course.getParts().size()));

		JPanel infoPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		infoPanel.setBackground(Color.orange);
		infoPanel.add(new JLabel("Course name: " + course.getCourseName()));
		infoPanel.add(new JLabel("Course description: " + course.getDescription()));
		subCoursePanel.add(infoPanel, BorderLayout.NORTH);
		
		JPanel mainBoxPanel = new JPanel();
		subCoursePanel.add(mainBoxPanel, BorderLayout.CENTER);
		mainBoxPanel.setLayout(new BoxLayout(mainBoxPanel, BoxLayout.PAGE_AXIS));
		
		for(Coursepart part : course.getParts()) {
			
			mainBoxPanel.add(getPartPanel(course, part, student, listen));
			mainBoxPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
		}
		return subCoursePanel;
		
	}
	
	/**
	 * Sets up a panel for each part of a given course part
	 * @param course, the given course
	 * @param part, the current part
	 * @param student, the selected students
	 * @param listen, the listener for the panels
	 * @return the new panel for the course part
	 */
	private static JPanel getPartPanel(Course course, Coursepart part, Student student, Listener listen) {
		
		JPanel outerCoursePanel = new JPanel(new GridLayout(1, 2));
		outerCoursePanel.setBackground(Color.white);
		JPanel coursePartPanel = new JPanel(new GridLayout(4, 2));
		coursePartPanel.setBackground(Color.white);
		
		coursePartPanel.add(new JLabel("Part description:"));
		coursePartPanel.add(new JLabel(part.getDescription()));
		
		coursePartPanel.add(new JLabel("Grade weight:"));
		coursePartPanel.add(new JLabel(String.valueOf(part.getGradeWeight())));
		
		coursePartPanel.add(new JLabel("Group members:"));
		JLabel members = new JLabel("");
		coursePartPanel.add(members);
		
		String comboString = course.getCourseName() + "|" + part.getDescription()+"|";
		coursePartPanel.add(new JLabel("Grade"));
		JComboBox<String> gradeCombo = new JComboBox<>(Validators.gradesAsArray());
		gradeCombo.addActionListener(listen);
		gradeCombo.setActionCommand("gradeCombo");
		
		gradeCombo.setBackground(Color.white);
		coursePartPanel.add(gradeCombo);
		
		
		Studentgroups group = part.findGroupByStudent(student);
		if(group != null) {
			
				members.setText(group.toString());
				
				Grade grade = group.getGrade();
				comboString += student.getName();
				if(grade != null) {
					gradeCombo.setSelectedIndex(gradeCombo.getItemCount() - grade.getGradeAsInt()-1);
				}
		}
		else {
			members.setText("No group registered");
			gradeCombo.setEnabled(false);
		}
		gradeCombo.setName(comboString);
		outerCoursePanel.add(coursePartPanel);
		
		JPanel fillerPanel = new JPanel();
		fillerPanel.setBackground(Color.white);
		outerCoursePanel.add(fillerPanel);
		
		return outerCoursePanel;
	}
	
	/**
	 * Sets up a panel of information with courses, courseparts and grades for the Studentgradecard
	 * @param student in question
	 * @return the new jpanel with course info
	 */
	public static JPanel updateStudentGradeCardInfoPanel(Student student) {
		
		JPanel infoPanel = new JPanel();
		infoPanel.setBackground(Color.white);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));
		for(Course course : student.getTakesCourse()) {
			JLabel courseLabel = new JLabel(course.getCourseName());
			courseLabel.setFont(new Font("Verdana", Font.BOLD, 16));
			infoPanel.add(courseLabel);
			for(Coursepart part : course.getParts()) {
				
				String description = part.getDescription();
				if(description.length() > 30) {
					description = description.substring(0, 30) + "....";
				}
				infoPanel.add(new JLabel(description));
				
				for(Studentgroups group : part.getGroups()) {
					if(group.getGroupMembers().contains(student)) {
						
						if(group.getGrade() != null) infoPanel.add(new JLabel(group.getGrade().getGradeAsString()));
						else infoPanel.add(new JLabel("No grade"));
					}
				}
				
			}
			JLabel finalGrade = new JLabel("Final grade: " + Gradecalculator.convertIntToGrade(Gradecalculator.getAverageGrade(student, course)).getGradeAsString());
			finalGrade.setForeground(Color.red);
			infoPanel.add(finalGrade);
			infoPanel.add(new JSeparator());
		}
		
		infoPanel.add(new JSeparator());
		infoPanel.add(new JLabel("Final grade overall: " + Gradecalculator.getTotalAverage(student)));
		
		return infoPanel;
	}
	

}

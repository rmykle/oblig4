package data;

import java.util.ArrayList;

/**
 * Class represents a course
 * @author rmy002
 *
 */
public class Course {
	
	private int courseID;
	private String courseName;
	private String description;
	private ArrayList<Coursepart> parts;
	
	
	/**
	 * Constructor for the class Course
	 * @param id - id of the course
	 * @param courseName - name of the course
	 * @param description - description of the course
	 */
	public Course(int id, String courseName, String description) {
		super();
		this.courseID = id;
		this.courseName = courseName;
		this.description = description;
		this.parts = new ArrayList<>();
	}
	
	/**
	 * Checks for any updates on the course information
	 * @param courseInput, array of new course information
	 * @return true of no changes are made
	 */
	public boolean checkUpdates(String[] courseInput) {
		
		if(!this.courseName.equals(courseInput[0]) || !this.description.equals(courseInput[1])) {
			this.courseName = courseInput[0];
			this.description = courseInput[1];
			return false;
		}
		return true;
		
	}
	
	/**
	 * Finds a course part given a part description
	 * @param desc of the part
	 * @return the course part
	 */
	public Coursepart getPartByDescription(String desc) {
		for(Coursepart part : parts) {
			if(part.getDescription().equals(desc)) {
				return part;
			}
		}
		return null;
	}
	
	/**
	 * Find a course part by ID
	 * @param id of the part
	 * @return the part
	 */
	public Coursepart findPartByID(int id) {
		for(Coursepart part : parts) {
			if(part.getId() == id) {
				return part;
			}
		}
		return null;
	}
	
	/**
	 * Methods gets the total grade weight assigned for the course
	 * @return sum of all part grade weights
	 */
	public int getTotalGradeWeight() {
		return parts.stream().mapToInt(i -> i.getGradeWeight()).sum();
		
	}
	
	/**
	 * Checks if the grade weight is 100%
	 * @return true if 100%
	 */
	public boolean checkWeightGrade() {
		if(getTotalGradeWeight() == 100) {
			return true;
		}
		return false;
	}
	
	/**
	 * Tostring of the course object
	 * @return courseName;
	 */
	@Override
	public String toString() {
		return courseName;
	}
	
	/**
	 * @return the courseName
	 */
	public String getCourseName() {
		return courseName;
	}
	/**
	 * @param courseName the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	/**
	 * @return the parts
	 */
	public ArrayList<Coursepart> getParts() {
		return parts;
	}
	/**
	 * @param parts the parts to set
	 */
	public void setParts(ArrayList<Coursepart> parts) {
		this.parts = parts;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the courseID
	 */
	public int getCourseID() {
		return courseID;
	}


	/**
	 * @param courseID the courseID to set
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}

}

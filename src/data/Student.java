package data;

import java.util.ArrayList;

/**
 * Class represents a student in the program
 * @author rmy002
 *
 */
public class Student {
	
	private int id;
	private String name;
	private ArrayList<Course> takesCourse;
	

	/**
	 * Constructor for the class Student
	 * @param id - id of the student
	 * @param name - name of the student
	 */
	public Student(int id, String name) {
		this.id = id;
		this.name = name;
		this.takesCourse = new ArrayList<>();
	}
	
	/**
	 * Methods checks if the student has removed a course when selecting new courses
	 * @param newCourses - the freshly selected courses
	 * @return an arraylist of missing courses
	 */
	public ArrayList<Course> compareCourses(ArrayList<Course> newCourses) {
		ArrayList<Course> oldCourses = new ArrayList<>(takesCourse);
		oldCourses.removeAll(newCourses);
		return oldCourses;
	}
	
	
	/**
	 * toString of the Student Object
	 */
	@Override 
	public String toString(){
		return name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}






	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the takesCourse
	 */
	public ArrayList<Course> getTakesCourse() {
		return takesCourse;
	}



	/**
	 * @param takesCourse the takesCourse to set
	 */
	public void setTakesCourse(ArrayList<Course> takesCourse) {
		this.takesCourse = takesCourse;
	}


}

package enums;

/**
 * Enum for the various grades on the assignments.
 * @author rmy002
 *
 */
public enum Grade {
	
	A(6, "A"), B(5, "B"), C(4, "C"), D(3, "D"), E(2, "E"), F(1, "F"), NOT_GRADED(0, "Missing part grades");
	
	private final int gradeAsInt;
	private final String gradeAsString;
	Grade(int gradeAsInt, String gradeAsString) {
		this.gradeAsInt = gradeAsInt;
		this.gradeAsString = gradeAsString;
	}
	/**
	 * @return the gradeAsInt
	 */
	public int getGradeAsInt() {
		return gradeAsInt;
	}
	/**
	 * @return the gradeAsString
	 */
	public String getGradeAsString() {
		return gradeAsString;
	}

}

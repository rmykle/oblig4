package interfaces;

public interface DAOInterface<U, L> {
	
	public void getAllInstances();
	public U getInstance(L id);
	public void updateInstance(U input);
	public void deleteInstance(U input);
	public int addnewInstance(U input);

}

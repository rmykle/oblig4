package sqlhandling;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

import data.Course;
import data.Storageunit;
import data.Student;
import interfaces.DAOInterface;

public class StudentcourseDAO implements DAOInterface<Integer[], Integer> {
	
	private static final String getAllStudentCoursesString = "SELECT * FROM Studentcourses"; 
	private static final String addNewStudentCourseString = "INSERT INTO Studentcourses VALUES(?,?)";
	private static final String deleteStudentCourseString = "DELETE FROM Studentcourses WHERE StudentID = ? AND CourseID = ?";
	
	PreparedStatement getAll;
	PreparedStatement addNewStudentCourse;
	PreparedStatement deleteStudentCourse;
	
	public StudentcourseDAO() {
		Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
		try {
			getAll = (PreparedStatement) connection.prepareStatement(getAllStudentCoursesString);
			addNewStudentCourse = (PreparedStatement) connection.prepareStatement(addNewStudentCourseString);
			deleteStudentCourse = (PreparedStatement) connection.prepareStatement(deleteStudentCourseString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	@Override
	public void getAllInstances() {
		Storageunit storage = Storageunit.getStorage();
		try {
						
			ResultSet results = (ResultSet) getAll.executeQuery();
			while(results.next())  {
				Student student = storage.getStudents().get(results.getInt(1));
				Course course = storage.getCourses().get(results.getInt(2));
				storage.addStudentToCourse(student, course);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public Integer[] getInstance(Integer id) {
		return null;
		
	}
	@Override
	public void updateInstance(Integer[] input) {
		
	}
	@Override
	public void deleteInstance(Integer[] input) {
		try {
			deleteStudentCourse.setInt(1, input[0]);
			deleteStudentCourse.setInt(2, input[1]);
			deleteStudentCourse.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	@Override
	public int addnewInstance(Integer[] input) {
		try {
			addNewStudentCourse.setInt(1, input[0]);
			addNewStudentCourse.setInt(2, input[1]);
			addNewStudentCourse.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	

}

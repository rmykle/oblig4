package sqlhandling;

import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;
import data.Storageunit;
import data.Student;
import interfaces.DAOInterface;

public class StudentDAO implements DAOInterface<Student, Integer>  {

	private static final String getAllStudentsString = "SELECT * FROM Student"; 
	private static final String updateStudentString = "UPDATE Course SET Name = ? WHERE CourseID = ?";
	private static final String inputNewStudentString = "INSERT INTO Student (Name) VALUES(?)";
	private static final String deleteStudentString = "DELETE FROM Student WHERE StudentID = ?";
	private static final String getStudentString = "SELECT * FROM Student WHERE StudentID = ?";
	
	PreparedStatement getAllStudents;
	PreparedStatement addNewStudent;
	PreparedStatement deleteStudent;
	PreparedStatement updateStudent;
	PreparedStatement getStudent;
	
	public  StudentDAO() {
		try {
			Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
			getAllStudents = (PreparedStatement) connection.prepareStatement(getAllStudentsString);
			addNewStudent = (PreparedStatement) connection.prepareStatement(inputNewStudentString);
			deleteStudent = (PreparedStatement) connection.prepareStatement(deleteStudentString);
			updateStudent = (PreparedStatement) connection.prepareStatement(updateStudentString);
			getStudent = (PreparedStatement) connection.prepareStatement(getStudentString);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	
	@Override
	public void getAllInstances() {
		try {
			Storageunit storage = Storageunit.getStorage();
			
			ResultSet results;
			results = (ResultSet) getAllStudents.executeQuery();
				
				while(results.next()) {
					
					storage.getStudents().put(results.getInt(1), new Student(results.getInt(1), results.getString(2)));
					
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
			
		
	}

	@Override
	public Student getInstance(Integer id) {
		Storageunit storage = Storageunit.getStorage();
		
	
		ResultSet result;
		try {
			getStudent.setInt(1, id);
			result = (ResultSet) getStudent.executeQuery();
			
			if(result.next()) {
				Student student = storage.getStudents().get(result.getInt(1));
				
				if(student == null) {
					student = new Student(result.getInt(1), result.getString(2));
					storage.getStudents().put(student.getId(), student);
				}
				
				return student;
				
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateInstance(Student input) {
		
		try {
			updateStudent.setString(1, input.getName());
			updateStudent.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteInstance(Student input) {
		try {
			deleteStudent.setInt(1, input.getId());
			deleteStudent.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public int addnewInstance(Student input) {
		
		try {
			addNewStudent.setString(1, input.getName());
			addNewStudent.executeUpdate();
			
			
			ResultSet idGenerated = (ResultSet) addNewStudent.getGeneratedKeys();
				if(idGenerated.next()) {
					return idGenerated.getInt(1);
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
		
		
		
	}

}

package sqlhandling;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;
import data.Course;
import data.Storageunit;
import interfaces.DAOInterface;

public class CourseDAO implements DAOInterface<Course, Integer> {
	
	private static final String getAllCoursesString = "SELECT * FROM Course"; 
	private static final String updateCourseString = "UPDATE Course SET Name = ?, Description = ? WHERE CourseID = ?";
	private static final String addNewCourseString = "INSERT INTO Course (Name, Description) VALUES (?, ?)";
	private static final String deleteCourseString = "DELETE FROM Course WHERE CourseID = ?";
	private static final String getCourseString = "SELECT * FROM Course WHERE CourseID = ?";
	
	PreparedStatement getAllCourses;
	PreparedStatement updateCourse;
	PreparedStatement newCourse;
	PreparedStatement deleteCourse;
	PreparedStatement getCourse;
	

	/**
	 * @param updateCourse
	 */
	public CourseDAO() {
	
		try {
			Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
			getAllCourses = (PreparedStatement) connection.prepareStatement(getAllCoursesString);
			updateCourse = (PreparedStatement) connection.prepareStatement(updateCourseString);
			newCourse = (PreparedStatement) connection.prepareStatement(addNewCourseString);
			deleteCourse = (PreparedStatement) connection.prepareStatement(deleteCourseString);
			getCourse = (PreparedStatement) connection.prepareStatement(getCourseString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void getAllInstances() {
		Storageunit storage = Storageunit.getStorage();
		
		ResultSet result;
		try {
			
			
			result = (ResultSet) getAllCourses.executeQuery();
			while(result.next()) {
				Course course = new Course(result.getInt(1), result.getString(2), result.getString(3));
				storage.getCourses().put(course.getCourseID(), course);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Course getInstance(Integer id) {
		ResultSet result;
		Storageunit storage = Storageunit.getStorage();
		try {
			
			getCourse.setInt(1, id);
			result = (ResultSet) getCourse.executeQuery();
			if(result.next()) {
				
				Course course = storage.getCourses().get(result.getInt(1));
				if(course == null) {
					course = new Course(result.getInt(1), result.getString(2), result.getString(3));
				}
				return course;
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void updateInstance(Course input) {
		
		try {
			updateCourse.setString(1, input.getCourseName());
			updateCourse.setString(2, input.getDescription());
			updateCourse.setInt(3, input.getCourseID());
			
			updateCourse.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
			
			
	
	}

	@Override
	public void deleteInstance(Course input) {
		try {
			deleteCourse.setInt(1, input.getCourseID());
			deleteCourse.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public int addnewInstance(Course input) {
		try {
			newCourse.setString(1, input.getCourseName());
			newCourse.setString(2, input.getDescription());
			newCourse.executeUpdate();
			ResultSet idGenerated = (ResultSet) newCourse.getGeneratedKeys();
			if(idGenerated.next()) {
				return idGenerated.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	



	

	
}

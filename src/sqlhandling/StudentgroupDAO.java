package sqlhandling;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;
import data.Coursepart;
import data.Storageunit;
import data.Student;
import data.Studentgroups;
import interfaces.DAOInterface;
import utilities.Validators;

public class StudentgroupDAO implements DAOInterface<Studentgroups, Integer>{

	private static final String getAllGroupsString = "SELECT Assignement.GroupID, Assignement.Grade, Assignement.CoursepartID, "
			+ "Studentgroup.StudentID FROM Assignement, Studentgroup WHERE Assignement.GroupID = Studentgroup.GroupID";
	private static final String addNewGroupString = "INSERT INTO Assignement (CoursepartID, GRADE) VALUES (?,?)"; 
	private static final String addMemberToGroup = "INSERT INTO Studentgroup VALUES(?,?)";
	private static final String updateGroupGradeString = "UPDATE Assignement SET Grade = ? WHERE GroupID = ?";
	private static final String deleteGroupString = "DELETE FROM Assignement WHERE GroupID = ?";
	private static final String removeGroupMemberString = "DELETE FROM Studentgroup WHERE GroupID = ? AND StudentID = ?";
	
	PreparedStatement getAllGroups;
	PreparedStatement addNewGroup;
	PreparedStatement addGroupMember;
	PreparedStatement updateGrade;
	PreparedStatement deleteGroup;
	PreparedStatement removeMember;
	
	public StudentgroupDAO() {
	
		Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
		try {
			getAllGroups = (PreparedStatement) connection.prepareStatement(getAllGroupsString);
			addNewGroup = (PreparedStatement) connection.prepareStatement(addNewGroupString);
			addGroupMember = (PreparedStatement) connection.prepareStatement(addMemberToGroup);
			updateGrade = (PreparedStatement) connection.prepareStatement(updateGroupGradeString);
			deleteGroup = (PreparedStatement) connection.prepareStatement(deleteGroupString);
			removeMember = (PreparedStatement) connection.prepareStatement(removeGroupMemberString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
		
	@Override
	public void getAllInstances() {
		Storageunit storage = Storageunit.getStorage();
		
		try {
			ResultSet results = (ResultSet) getAllGroups.executeQuery();
			
			int groupID;
			String grade;
			int studentID;
			
			while(results.next()) {
				
				groupID = results.getInt(1);
				grade = results.getString(2);
				Coursepart coursePart = storage.getCoursePartByID(results.getInt(3));
				studentID = results.getInt(4);
				
				Studentgroups group = coursePart.findGroupByID(groupID);
				if(group == null) {
					ArrayList<Student> student = new ArrayList<>();
					student.add(storage.getStudents().get(studentID));
					Studentgroups newGroup = new Studentgroups(student, coursePart);
					newGroup.setId(groupID);
					if(grade != null) newGroup.setGrade(Validators.gradeStringToGrade(grade));
					coursePart.getGroups().add(newGroup);
				}
				else {
					group.getGroupMembers().add(storage.getStudents().get(studentID));
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Studentgroups getInstance(Integer id) {
		return null;
		
	
		
	}

	@Override
	public void updateInstance(Studentgroups input) {
		try {
			updateGrade.setString(1, input.getGrade().getGradeAsString());
			updateGrade.setInt(2, input.getId());
			updateGrade.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteInstance(Studentgroups input) {
		
		try {
			System.out.println("test gruppeslett");
			deleteGroup.setInt(1, input.getId());
			deleteGroup.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int addnewInstance(Studentgroups input) {
		
		int newId = 0;
		try {
			addNewGroup.setInt(1, input.getPart().getId());
			if(input.getGrade() != null) {
				addNewGroup.setString(2, input.getGrade().getGradeAsString());
			}
			else {
				addNewGroup.setNull(2, Types.VARCHAR);
			}
			
			addNewGroup.executeUpdate();
			
			ResultSet id = (ResultSet) addNewGroup.getGeneratedKeys();
			if(id.next()) {
				newId = id.getInt(1);
			}
			input.setId(newId);
			for(Student student : input.getGroupMembers()) {
				addNewGroupMember(student, input);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return newId;
	}
	
	public void addNewGroupMember(Student student, Studentgroups group) {
		try {
			addGroupMember.setInt(1, group.getId());
			addGroupMember.setInt(2, student.getId());
			addGroupMember.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void removeGroupMember(Studentgroups group, Student student) {
		try {
			removeMember.setInt(1, group.getId());
			removeMember.setInt(2, student.getId());
			removeMember.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}


	

}

package sqlhandling;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Connection;

public class SqlDAOFactory extends DAOfactory{
	private static SqlDAOFactory sqlFactory = null;
	
	private static String driver = "com.mysql.jdbc.Driver";
	private Connection connection = null;
	private static final String host = "bigfoot.uib.no";
	private static final String dbName = "gr2_16";
	private static final int port = 3306;
	private static String mySqlUrl = "jdbc:mysql://" + host + ":" + port + "/" + dbName;
	
	
	public Connection createConnection() {
		
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.err.println("RIP driverfeil");
		}
		
		Properties userInfo = new Properties();	
		userInfo.put("user", "i233_16_gr2");
		userInfo.put("password", "tyhdfy");
		
		
		
		try {
			connection = (Connection) DriverManager.getConnection(mySqlUrl, userInfo);
			return connection;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	private SqlDAOFactory() {
		
	}
	

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the mySqlUrl
	 */
	public String getMySqlUrl() {
		return mySqlUrl;
	}

	@Override
	public CourseDAO getCourseDAO() {
		return new CourseDAO();
	}



	/**
	 * @return the driver
	 */
	public static String getDriver() {
		return driver;
	}



	/**
	 * @param driver the driver to set
	 */
	public static void setDriver(String driver) {
		SqlDAOFactory.driver = driver;
	}


	/**
	 * @return the dbname
	 */
	public static String getDbname() {
		return dbName;
	}



	/**
	 * @param mySqlUrl the mySqlUrl to set
	 */
	public static void setMySqlUrl(String mySqlUrl) {
		SqlDAOFactory.mySqlUrl = mySqlUrl;
	}



	@Override
	public CoursepartDAO getCoursepartDAO() {
		return new CoursepartDAO();
	}



	@Override
	public StudentDAO getStudentDAO() {
		return new StudentDAO();
	}



	@Override
	public StudentcourseDAO getStudentCourseDAO() {
		return new StudentcourseDAO();
	}



	@Override
	public StudentgroupDAO getStudentGroupDAO() {
		return new StudentgroupDAO();
	}



	/**
	 * @return the sqlFactory
	 */
	public static SqlDAOFactory getSqlFactory() {
		if(sqlFactory == null) {
			sqlFactory = new SqlDAOFactory();
		}
		return sqlFactory;
	}



	/**
	 * @param sqlFactory the sqlFactory to set
	 */
	public static void setSqlFactory(SqlDAOFactory sqlFactory) {
		SqlDAOFactory.sqlFactory = sqlFactory;
	}
	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}
	/**
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}





	
	

}

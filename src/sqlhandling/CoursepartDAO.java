package sqlhandling;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;
import data.Course;
import data.Coursepart;
import data.Storageunit;
import interfaces.DAOInterface;

public class CoursepartDAO implements DAOInterface<Coursepart, Integer> {
	
	private final String getAllString = "SELECT * FROM Coursepart";
	private static final String updatePartString = "UPDATE Coursepart SET Description = ?, Groupsize = ? , Gradeweight = ? WHERE CoursepartID = ?";
	private static final String deleteCoursePartString = "DELETE FROM Coursepart WHERE CoursepartID = ?";
	private static final String addNewPartString = "INSERT INTO Coursepart(Course, Description, Groupsize, Gradeweight) VALUES (?,?,?,?)";
	private static final String getPartString = "SELECT * FROM Coursepart WHERE ID = ?";
	
	PreparedStatement getAllParts;
	PreparedStatement updatePart;
	PreparedStatement deletePart;
	PreparedStatement addCoursePart;
	PreparedStatement getPart;
	
	public CoursepartDAO() {
		
	Connection connection = SqlDAOFactory.getSqlFactory().getConnection();
	try {
		getAllParts = (PreparedStatement) connection.prepareStatement(getAllString);
		updatePart = (PreparedStatement) connection.prepareStatement(updatePartString);
		deletePart = (PreparedStatement) connection.prepareStatement(deleteCoursePartString);
		addCoursePart = (PreparedStatement) connection.prepareStatement(addNewPartString);
		getPart = (PreparedStatement) connection.prepareStatement(getPartString);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	}

	@Override
	public void getAllInstances() {
		Storageunit storage = Storageunit.getStorage();
				
		ResultSet result;
		try {
			
			result = (ResultSet) getAllParts.executeQuery();
			
			while(result.next()) {
				
				int id = result.getInt(1);
				int courseId = result.getInt(2);
				String courseDescription = result.getString(3);
				int groupSize = result.getInt(4);
				int gradeWeight = result.getInt(5);
				
				Course course = storage.getCourses().get(courseId);
				Coursepart part = new Coursepart(id, course, courseDescription, groupSize, gradeWeight);
				course.getParts().add(part);
				
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Coursepart getInstance(Integer id) {
		Storageunit storage = Storageunit.getStorage();
		try {
			getPart.setInt(1, id);
			ResultSet result = (ResultSet) getPart.executeQuery();
			if(result.next()) {
				Coursepart current = storage.getCoursePartByID(result.getInt(1));
				if(current == null) {
					current = new Coursepart(result.getInt(1), storage.getCourses().get(result.getInt(2)), result.getString(3), result.getInt(4), result.getInt(5));
				}
				return current;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public void updateInstance(Coursepart input) {
		
		try {
			updatePart.setString(1, input.getDescription());
			updatePart.setInt(2, input.getGroupSize());
			updatePart.setInt(3, input.getGradeWeight());
			updatePart.setInt(4, input.getId());
			updatePart.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void deleteInstance(Coursepart input) {
		
		try {
			deletePart.setInt(1, input.getId());
			deletePart.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public int addnewInstance(Coursepart input) {
		
		try {
			addCoursePart.setInt(1, input.getPartOf().getCourseID());
			addCoursePart.setString(2, input.getDescription());
			addCoursePart.setInt(3, input.getGroupSize());
			addCoursePart.setInt(4, input.getGradeWeight());
			addCoursePart.executeUpdate();
			ResultSet id = (ResultSet) addCoursePart.getGeneratedKeys();
			if(id.next()) {
				return id.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}


}

package sqlhandling;

import enums.DAOtype;

public abstract class DAOfactory {
	
	public abstract CourseDAO getCourseDAO();
	public abstract CoursepartDAO getCoursepartDAO();
	public abstract StudentDAO getStudentDAO();
	public abstract StudentcourseDAO getStudentCourseDAO();
	public abstract StudentgroupDAO getStudentGroupDAO();
	
	public static DAOfactory getDAOFactory(DAOtype type) {
		
		switch (type) {
		case SQL:
			
			return SqlDAOFactory.getSqlFactory();

		default:
			return null;
		}
		
	}

}

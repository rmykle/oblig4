package utilities;

import java.util.ArrayList;

import data.Course;
import enums.Grade;

public abstract class Validators {
	
	public static Object[] convertCoursePartInput(String[] input) {
		if(!checkStringAsInt(input[2]) && !checkStringAsInt(input[3])) {
		return null;
		}
		Object[] validArray = new Object[input.length];
			
			validArray[0] = Integer.parseInt(input[0]);
			validArray[1] = (String) input[1];
			validArray[2] = Integer.parseInt(input[2]);
			validArray[3] = Integer.parseInt(input[3]);

		
		return validArray;
	}
	
	public static boolean checkEmptyArray(String[] input) {
		for(int i = 0; i < input.length; i++) {
			if(input[i].isEmpty()) return true;
		}
		return false;
	}
	public static boolean checkStringAsInt(String input) {
		return  input.matches("[0-9]+");
	}
	
	public static Grade gradeStringToGrade(String gradeString) {
		if (gradeString!= null) {
			if (gradeString.toUpperCase().matches("[A-F]")) {
				Grade grade = Grade.valueOf(gradeString.toUpperCase());
				return grade;
			}
		}
		return null;

	}
	
	
	
	
	public static ArrayList<Course> compareCourses(ArrayList<Course> newCourses) {
		
		return null;
	}
	
	public static String[] gradesAsArray() {
		Grade[] enums = Grade.values();
		String[] grades = new String[enums.length+1];
		grades[0] = "";
		for(int i = 0; i < enums.length; i++) {
			grades[i+1] = enums[i].getGradeAsString();
		}
		return grades;
	}

}

package utilities;

import java.util.ArrayList;

import GUI.GUI;
import GUI.Listener;
import data.Course;
import data.Coursepart;
import data.Storageunit;
import data.Student;
import data.Studentgroups;
import enums.DAOtype;
import enums.Grade;
import sqlhandling.CourseDAO;
import sqlhandling.CoursepartDAO;
import sqlhandling.DAOfactory;
import sqlhandling.SqlDAOFactory;
import sqlhandling.StudentDAO;
import sqlhandling.StudentcourseDAO;
import sqlhandling.StudentgroupDAO;

public class Main {

	
	public static void main(String[] args) {
		Storageunit storage = Storageunit.getStorage();
		Listener listen = new Listener(storage);
	
		SqlDAOFactory sqlFactory = 
				(SqlDAOFactory) DAOfactory.getDAOFactory(DAOtype.SQL);
		listen.setDaoFactory(sqlFactory);
		sqlFactory.createConnection();
		
		StudentDAO studentDao = sqlFactory.getStudentDAO();
		studentDao.getAllInstances();
		CourseDAO courseDao = sqlFactory.getCourseDAO();
		courseDao.getAllInstances();
		CoursepartDAO part = sqlFactory.getCoursepartDAO();
		part.getAllInstances();
		StudentcourseDAO studentCourseDAO = sqlFactory.getStudentCourseDAO();
		studentCourseDAO.getAllInstances();
		StudentgroupDAO studentGroupDao = sqlFactory.getStudentGroupDAO();
		studentGroupDao.getAllInstances();
		
		
		
		
		GUI gui = GUI.getGui(listen);
		listen.setGui(gui);
		gui.getEditCard().paintPartPanels();
		
	
			
		}
	
	public void addFillerData(Storageunit storage) {
		Course course = new Course(8, "BAK101", "Bak en digg kake");
		Coursepart part1 = new Coursepart(5, course, "Ta i gj�r", 1, 20);
		Coursepart part2 = new Coursepart(7, course, "Stek kaken", 4, 20);
		Coursepart part3 = new Coursepart(12, course, "Eksamen", 1, 60);
		
		course.getParts().add(part1);
		course.getParts().add(part2);
		course.getParts().add(part3);
		
		storage.getCourses().put(8, course);
		
		Course ecoC = new Course(9, "ECO100", "Skaff kredittkort");
		Coursepart eco1 = new Coursepart(10, ecoC, "Lei en bil del 1", 2, 20);
		Coursepart eco2 = new Coursepart(11, ecoC, "Lei en vaskemaskin del 2", 1, 30);
		Coursepart eco3 = new Coursepart(140, ecoC, "Eksameasdasdasdasdasdasdasdasdasdn", 1, 50);

		ecoC.getParts().add(eco1);
		
		ecoC.getParts().add(eco2);
		ecoC.getParts().add(eco3);
		storage.getCourses().put(9, ecoC);
		
		Student student1 = new Student(1, "Birger");
		Student student2 = new Student(2, "Kato");
		Student student3 = new Student(3, "Andres");
		
		storage.getStudents().put(1, student1);
		storage.getStudents().put(2, student2);
		storage.getStudents().put(3, student3);
		
		storage.addStudentToCourse(student2, ecoC);
		storage.addStudentToCourse(student2, course);
		
		storage.addStudentToCourse(student1, course);
		
		storage.addStudentToCourse(student3, ecoC);
		storage.addStudentToCourse(student3, course);
		storage.addStudentToCourse(student1, ecoC);
		
		
		ArrayList<Student> studentz = new ArrayList<>();
		studentz.add(storage.getStudentByName("Kato"));
		studentz.add(storage.getStudentByName("Birger"));
		Studentgroups group1 = new Studentgroups(studentz, part2);
		group1.setGrade(Grade.F);
		part2.getGroups().add(group1);
		
	}

	
}
